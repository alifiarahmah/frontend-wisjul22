<h1 align="center">Frontend-Wisjul22</h1>

<div align="center">

![React JS](https://img.shields.io/badge/React-20232A?style=flat&logo=react&logoColor=61DAFB)
![Chakra UI](https://img.shields.io/badge/Chakra--UI-319795?style=flat&logo=chakra-ui&logoColor=white)
[![Google Analytics](https://img.shields.io/badge/Google%20Analytics-E37400?style=flat&logo=google%20analytics&logoColor=white)](https://drive.google.com/file/d/17CGhmdKLz5XoFU4_Bmc0L4x8N4GPmkAU/view?usp=sharing)
[![Figma](https://img.shields.io/badge/Figma-F24E1E?style=flat&logo=figma&logoColor=white)](https://www.figma.com/file/UrnloLczTtEhMBk5YeY0k2/Design-Website-Wisjul-2022?node-id=64%3A20)
[![Netlify Status - Production](https://api.netlify.com/api/v1/badges/7db7c9a1-90c7-49fb-b229-cb97240b9386/deploy-status?branch=main)](https://paradewisudaitb.netlify.app)

</div>

<br/>

Repositori untuk kode Frontend dari website Perayaan Wisuda Juli 2022. Dikembangkan oleh panitia divisi website.

## Table of Contents

- [Fitur](#fitur)
- [Dokumentasi](#dokumentasi)
- [Development Notes](#development-notes)

## Fitur

### Landing Page

Halaman utama. Berisi informasi utama terkait Wisuda Juli dan navigasi menuju fitur-fitur utama website.

### About Us

Berisi visi dan misi Perayaan Wisuda Juli 2022 dan organogram panitia.

### TA Fair

Berisi halaman pameran TA Fair online dan denah untuk TA Fair offline. Tiap halaman berisi abstrak, portofolio, data diri wisudawan, dan kolom komentar. Terdapat pula fitur untuk menambahkan komentar untuk tiap tugas akhir menggunakan Google Sign-In.

### Wall of Appreciation

Berisi kisah inspiratif para wisudawan selama kuliah.

### Wisudawan

Berisi daftar wisudawan yang dikategorikan berdasar himpunannya. Tiap halaman wisudawan berisi foto, data diri wisudawan, dan kolom komentar untuk memberikan ucapan untuk masing-masing wisudawan dengan autentikasi menggunakan Google Sign-In.

## Dokumentasi

### Landing Page

![image](https://user-images.githubusercontent.com/28982967/181008149-28dc3c3c-73ca-4df5-b224-3c6b4bf46994.png)

![image](https://user-images.githubusercontent.com/28982967/181010147-e5f1c184-4577-43c8-8b84-42bdce1902cc.png)

### About Us

![image](https://user-images.githubusercontent.com/28982967/181009187-139e331c-fd9c-48d9-ae68-be731dc709a3.png)

![image](https://user-images.githubusercontent.com/28982967/181009118-3294dff2-3180-4924-96a8-da7769c9200c.png)

### TA Fair

![image](https://user-images.githubusercontent.com/28982967/181008561-9f4d0425-aa13-4713-9f93-f0c242a9bef2.png)

![image](https://user-images.githubusercontent.com/28982967/181009322-8602f80c-f643-4927-951a-ab41b326cbbb.png)

![image](https://user-images.githubusercontent.com/28982967/181009378-e2c716c3-11a1-4c85-9313-8e9909dea13f.png)

![image](https://user-images.githubusercontent.com/28982967/181009424-c8a71f21-a5f8-4019-a667-8cbeb8cab129.png)

### Wall of Appreciation

![image](https://user-images.githubusercontent.com/28982967/181009560-cb5a9b6e-6d7c-4393-8bcd-85a0d338942c.png)

![image](https://user-images.githubusercontent.com/28982967/181009781-c1882235-44ee-4a25-8025-8a4e755e29bb.png)

### Wisudawan

![image](https://user-images.githubusercontent.com/28982967/181008473-3be57cc4-0fa4-4e24-873e-c93fb9cdf2b2.png)

![image](https://user-images.githubusercontent.com/28982967/181009926-7e784c10-757a-4393-b4cd-89119c08af16.png)

## Development Notes

Pengerjaan website dilakukan dengan menggunakan Git Feature Branch Workflow.

Cara kerja:

1. Buat branch baru dari develop, dengan penamaan `feature/<nama-feature>`
2. Buat perubahan
3. Push perubahan ke branch baru, merge dulu sama develop
4. Pull request dari branch feature ke develop
5. Merge pull request ke develop kalau sudah di-approve
   Nanti dari develop ke main dihandle sama kita
