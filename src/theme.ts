import { extendTheme, withDefaultColorScheme } from "@chakra-ui/react";

export const theme = extendTheme(
  {
    colors: {
      // sesuai dengan library figma
      orange: {
        light: {
          default: "#FFA87D",
          hover: "#F5A076",
          active: "#ED996F",
        },
        normal: {
          default: "#F88848",
          hover: "#ED7E3E",
          active: "#E57737",
        },
        dark: {
          default: "#E37A3D",
          hover: "#D47037",
          active: "#C96B34",
        },
      },
      blue: {
        light: {
          default: "#8DC2FC",
          hover: "#82B2E8",
          active: "#7CABDE",
        },
        normal: {
          default: "#5C8DD6",
          hover: "#5481C4",
          active: "#507ABA",
        },
        dark: {
          default: "#4776B2",
          hover: "#3D68A1",
          active: "#386094",
        },
      },
      yellow: {
        light: "#FFE37F",
        normal: "#F9C710",
        dark: "#EBA900",
      },
      red: {
        light: "#FF824D",
        normal: "#FD4102",
        dark: "#DD0303",
      },
      green: {
        light: "#7FCEAF",
        normal: "#4FA5B0",
        dark: "#2F8092",
      },
      pink: {
        light: "#EEB8E9",
        normal: "#EC8CCB",
        dark: "#CF619C",
      },
      gray: {
        100: "#ECECEC",
        200: "#E2E2E2",
        300: "#C5C5C5",
        400: "#AEAEAE",
        500: "#878787",
        600: "#777777",
        700: "#5A5A5A",
        800: "#474747",
        900: "#262626",
      },
      bg: {
        light: "#FFFAE6",
        dark: "#2D4A58",
      },
      krem: {
        // disimpen aja dulu
        1: "#F3EDD5",
        2: "#FFFAE6",
      },
    },
    fonts: {
      body: "Montserrat, Roboto, system-ui, sans-serif",
      heading: "Milk Carton, Montserrat, Roboto, system-ui, sans-serif",
    },
  },
  withDefaultColorScheme({
    colorScheme: "orange",
  })
);
