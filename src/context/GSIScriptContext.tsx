import React, { createContext, useEffect, useState } from "react";

const GSIScriptContext = createContext(false);

export function GSIScriptContextProvider({
  children,
}: {
  children: React.ReactNode;
}) {
  const [isGSILoaded, setGSILoaded] = useState(false);

  useEffect(() => {
    const script = document.createElement("script");
    script.src = "https://accounts.google.com/gsi/client";
    script.id = "gsi-client";
    script.onload = () => {
      setGSILoaded(true);
    };
    script.async = true;
    script.defer = true;

    document.body.appendChild(script);

    return () => {
      document.body.removeChild(script);
    };
  }, []);

  return (
    <GSIScriptContext.Provider value={isGSILoaded}>
      {children}
    </GSIScriptContext.Provider>
  );
}

export default GSIScriptContext;
