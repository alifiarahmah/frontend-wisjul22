import React, { createContext, useState } from "react";
import IUser from "../model/IUser";
export interface AuthContextProps {
  token: string | null;
  user: IUser | null;
  setToken?: (token: string) => void;
  setUser?: (token: IUser) => void;
  destroyUser?: () => void;
}

const AuthContext = createContext<AuthContextProps>({
  token: null,
  user: null,
});

export function AuthProvider({ children }: { children: React.ReactNode }) {
  const [token, setToken] = useState<string | null>(null);
  const [user, setUser] = useState<IUser | null>(null);

  const destroyUser = () => {
    setToken(null);
    setUser(null);
  };

  return (
    <AuthContext.Provider
      value={{ token, setToken, user, setUser, destroyUser }}
    >
      {children}
    </AuthContext.Provider>
  );
}

export default AuthContext;
