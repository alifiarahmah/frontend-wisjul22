import React from 'react';
import { useState, useEffect } from 'react';
import Layout from '../../components/layout/Layout';
import { motion } from 'framer-motion';
import { Box, Flex, Text, IconButton, Image } from '@chakra-ui/react';
import { ArrowForwardIcon } from '@chakra-ui/icons';

import Loading from '../../components/Loading';
import liquid from '../../assets/images/tafair/liquid.png'
import TAFairAboutContent from './TAFairAboutContent';

function TAFairAbout() {
    const [showPlaylist, setShowPlaylist] = useState(true);
    const container = {
        hidden: { opacity: 0 },
        show: {
            opacity: 1,
            transition: {
                staggerChildren: 0.8
            }
        }
    }
    const item = {
        hidden: { opacity: 0 },
        show: { opacity: 1 }
    }
    const topFunction = () => {
        document.body.scrollTop = 0; // For Safari
        document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
    }
    const [loading, setLoading] = useState(true);
    useEffect(() => {
        const loadData = async () => {
            await setTimeout(() => {
                setLoading(false);
            }
                , 1000);
        }
        loadData();
    }
        , []);

    if (loading) {
        return (
            <Layout navbarvar="krem">
                <Loading />
            </Layout>
        );
    }
    return(
        <Layout navbarVar={showPlaylist ? "krem transparent" : "krem"} bg={showPlaylist ? "darkblue" : "krem"} noFooter>
            <Flex
                height="100vh"
                width="full"
                position="absolute"
                top="0px"
                fontWeight="500" 
                fontSize={{ base: "18px", md: "lg" }}
                hidden = {showPlaylist ? false : true}
            >
                <Image alt="" src={liquid} position="absolute" right="0" bottom="0" width="100vw" height="auto" />
                <Flex
                    height="100vh"
                    width="full"
                    position="absolute"
                    top="0px"
                    justifyContent="center"
                    alignItems="center"
                    textAlign="center"
                >
                    <motion.div
                        variants={container}
                        initial="hidden"
                        animate="show"
                    >
                        <motion.div variants={item}>
                            <Text padding="15px" textColor="krem.1"
                            >
                                Dalam menjalani hari-hari penuh berjuangan, <br />
                                terkadang kita perlu tahu bahwa kita tidak sendirian,
                            </Text>
                        </motion.div>
                        <motion.div variants={item}>
                            <Text padding="15px" textColor="krem.1"
                            >
                                Sesuatu untuk menemani kamu menulusuri pameran; <br />
                                berikut lagu-lagu yang bermakna bagi para wisudawan:
                            </Text>
                        </motion.div>
                        <motion.div variants={item}>
                            <Box
                                as="iframe"
                                src="https://open.spotify.com/embed/playlist/7fVq7WD3ITEt2nBmLcm7Il?utm_source=generator&theme=0"
                                width={{ base: "300px", md: "400px" }}
                                borderRadius="12px"
                                height={{ base: "300px", md: "400px" }}
                                allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"
                                mx="auto"
                                my={2}
                            >

                            </Box>
                        </motion.div>
                        <motion.div variants={item}>
                            <IconButton
                                colorScheme="orange"
                                aria-label="Forward"
                                icon={< ArrowForwardIcon />}
                                borderRadius="100%"
                                width="60px"
                                height="60px"
                                position="absolute"
                                fontSize="30px"
                                bottom="30px"
                                right="30px"
                                onClick={() => {setShowPlaylist(false) ; topFunction();}}
                            />
                        </motion.div>
                    </motion.div>
                </Flex>
            </Flex>
            <Box hidden={showPlaylist ? true : false}>
                <TAFairAboutContent />
            </Box>
        </Layout>
    )
}

export default TAFairAbout