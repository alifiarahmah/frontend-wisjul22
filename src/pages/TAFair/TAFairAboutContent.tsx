import React from 'react';
import { Box, Flex, Text,Image, Heading, Button } from '@chakra-ui/react';
import api from '../../assets/images/tafair/api.png'
import bintang from '../../assets/images/tafair/bintang.png'
import confetti from '../../assets/images/tafair/confetti.png'
import pansies from '../../assets/images/tafair/pansies.png'
import sun1 from '../../assets/images/tafair/sun1.png'
import sun2 from '../../assets/images/tafair/sun2.png'
import swirl from '../../assets/images/tafair/swirl.png'
import tulip from '../../assets/images/tafair/tulip.png'
import tulipMerah from '../../assets/images/tafair/Tulip merah.png'
import tulipKuning from '../../assets/images/tafair/tulip kuning.png'
import FadeInSection from '../../components/tafair/FadeInSection';
import { motion } from 'framer-motion';
import { Link } from 'react-router-dom';
import { ArrowForwardIcon } from '@chakra-ui/icons';

function TAFairAboutContent(){
    const container = {
        hidden: { opacity: 0, y:24 },
        show: {
            opacity: 1, y: 0,
            transition: {
                duration: 1,
                staggerChildren: 2
            }
        }
    }

    const item = {
        hidden: { opacity: 0,y: 24 },
        show: { 
            opacity: 1, y:0,
            transition: {
                duration: 2
            } 
        }
    }
    return(
        <Box overflow="hidden">
            <FadeInSection>
                <Box pos="relative" mt={10} mb="150px" fontWeight="500">
                    <motion.div variants={container} initial="hidden" animate="show">
                        <Image alt="" src={sun1} pos="absolute" left="0" top="100" maxWidth={{ base: "70%", md: "30vw" }} />
                        <Heading px="5vw" pt="1vw" fontSize={{ base: "8vw", md: "4vw" }} ml="10px">
                            TENTANG PAMERAN <br />
                            TUGAS AKHIR
                        </Heading>
                        <Flex
                            justifyContent={{ base: "center", md: "right" }}
                            textAlign="center"
                            width="full"
                            mt={{ base: "350px", md: "30px" }}
                            fontSize={{ base: "20", md: "24", lg: "36" }}
                            flexDir="column"
                        >
                            <motion.div variants={item}>
                                <Text my="50px" pt={{ base: "12vw", md: "0vw" }} ml={{ base: "0", md: "40vw" }} mx={{base:"5px", md:"0"}}>Selembar kertas, pipih tanpa tekukan
                                    <br /> Selembar kertas, baru tanpa goresan</Text>
                            </motion.div>
                            <motion.div variants={item}>
                                <Text my="50px" pt={{ base: "12vw", md: "0vw" }} ml={{ base: "0", md: "30vw" }} mx={{base:"5px", md:"0"}}>Lugu, menunggu terukirnya cerita baru
                                    <br /> Tenang, menanti apa yang akan datang</Text>
                            </motion.div>
                        </Flex>
                    </motion.div>
                </Box>
            </FadeInSection>
            <FadeInSection>
                <Box pos="relative" fontWeight="500">
                    <Image alt="" src={api} pos="absolute" right="0" maxWidth={{ base: "70%", md: "30vw" }} transform={{ base: "translate(0, -200px)", md: "translate(0, -30%)" }} zIndex="0" />
                    <Flex
                        justifyContent={{ base: "center", md: "left" }}
                        width="full"
                        fontSize={{ base: "20", md: "24", lg: "36" }}
                        flexDir="column"
                        pt={{ base: "300px", md: "0" }}
                        pb="350px"
                    >
                        <FadeInSection><Text my="10px" ml={{ base: "80px", md: "15vw" }} >Terlipat, tertekuk,</Text></FadeInSection>
                        <FadeInSection><Text my="10px" ml={{ base: "120px", md: "25vw" }} >Tergores, tertekan</Text></FadeInSection>
                        <FadeInSection><Text my="10px" ml={{ base: "30vw", md: "20vw" }} pt={{ base: "100px", md: "0" }}>Percaya bahwa ini akan berlalu,</Text></FadeInSection>
                        <FadeInSection><Text my="10px" ml={{ base: "40vw", md: "30vw" }} >meski menyakitkan</Text></FadeInSection>
                    </Flex>
                    <Image alt="" src={bintang} pos="absolute" bottom="0" maxWidth={{ base: "70%", md: "30vw" }} zIndex="0" />
                </Box>
            </FadeInSection>
            <FadeInSection>
                <Box pos="relative" fontWeight="500" >
                    <Image alt="" src={confetti} pos="absolute" right={{ base: "0", md:"initial" }} top={{base:"initial", md:"0"}} bottom={{base:"0", md:"initial"}} left={{base:"initial", md: "0" }} maxWidth={{ base: "50%", md: "30vw" }} transform={{
                        sm: "translate(30%, 0) rotate(-39.42deg)", md: "translate(0, -50%)"
                    }} zIndex="0" />
                    <Flex
                        justifyContent={{ base: "center", md: "right" }}
                        width="full"
                        fontSize={{ base: "20", md: "24", lg: "36" }}
                        flexDir="column"
                        pb="150px"
                    >
                        <Text ml={{ base: "12vw", md: "45vw" }} pr="20">
                            Setelah melalui proses <br />
                            transformasi panjang, <br />
                            kini para wisudawan telah <br />
                            menghasilkan karya-karya <br />
                            yang luar biasa.
                        </Text>
                    </Flex>
                </Box>
            </FadeInSection>
            <FadeInSection>
                <Box pos="relative" fontWeight="500">
                    <Image alt="" src={swirl} top="0" float={{ base: "left", md: "right" }} maxWidth={{ base: "40%", md: "30vw" }} transform={{ base: "matrix(-0.97, -0.25, -0.25, 0.97, 0, 0)", md: "translate(0, -30%)" }} zIndex="0" />
                    <Flex px={{ base: "5vw", md: "10vw" }} pt={{ base: "100px", md: "0" }}>
                        <Text float={{ base: "right", md: "left" }} fontSize={{ base: "18", md: "24", lg: "36" }} pb="200px" >
                            Pameran Tugas Akhir adalah salah satu bentuk
                            apresiasi kami terhadap wisudawan/wisudawati yang
                            telah menyelesaikan masa studinya di ITB dan kini
                            telah berhasil menyusun tugas akhirnya.
                        </Text>
                    </Flex>
                </Box>
            </FadeInSection>
            <FadeInSection>
                <Box pos="relative" fontWeight="500">
                    <Image alt="" src={sun2} top="0" float={{ base: "right", md: "left" }} maxWidth={{ base: "40%", md: "40vw" }} transform={{ base: "scaleX(-1)", md: "translate(0, -30%)" }} />
                    <Flex px={{ base: "5vw", md: "10vw" }} pt={{ base: "20px", md: "0" }} width={{ base: "full", md: "auto" }}>
                        <Text float={{ base: "left", md: "right" }} fontSize={{ base: "20", md: "24", lg: "36" }} pb="300px" mx={{base:"20px", md:"0"}} >
                            Pameran ini berisi sejumlah karya tugas akhir dari
                            wisudawan/wisudawati beserta rekapan momen
                            perkuliahan, cerita wisudawan, kesan pesan di ITB,
                            dan sebagainya.
                        </Text>
                    </Flex>
                </Box>
            </FadeInSection>
            <FadeInSection>
                <Box pos="relative" fontWeight="500">
                    <Image alt="" src={pansies} top="0" float={{ base: "left", md: "right" }} maxWidth={{ base: "40%", md: "30vw" }} transform={{ base: "scaleX(-1) translate(0,-70%)", md: "translate(0, -60%)" }} />
                    <Flex px={{ base: "2vw", md: "10vw" }} pt={{ base: "0", md: "0" }}>
                        <Text float={{ base: "right", md: "left" }} fontSize={{ base: "20", md: "24", lg: "36" }} pb={{base:"400px", md:"300px"}} >
                            Semoga kakak-kakak wisudawan/wisudawati dan
                            keluarga bisa berbangga terhadap karya-karyanya,
                            dan semoga tonggak pencapaian ini menjadi batu loncatan
                            untuk semakin banyak kesuksesan-kesuksesan lainnya bagi kakak-kakak.
                        </Text>
                    </Flex>
                </Box>
            </FadeInSection>
            <FadeInSection>
                <Box pos="relative">
                    <Heading textAlign="center" fontSize={{ base: "10vw", md: "4vw", lg:"8vw" }} py={10}>Selamat atas <br/> kelulusannya!</Heading>
                    <Image alt="" maxWidth= {{base:"60%", md:"100%"}}pos="absolute" right={{base:"0", md:"initial"}} left={{base:"initial", md:"0"}} src={tulip} transform={{ base: "scaleX(-1) translate(0, -150%)", md: "translate(0, -30%)" }} />
                </Box>
            </FadeInSection>
            <FadeInSection>
                <Box flexDir="row" justifyContent={{ base: "center", md: "flex-end" }} width="full" height="500px" textAlign="center" fontWeight="500">
                    {/* <Link to="/ta-fair/main">
                        <Box width="200px" height="200px">
                            <Image alt="" src={tulipKuning} transform="translateX(10%)" />
                            <Text>Halaman <br /> Utama</Text>
                        </Box>
                    </Link> */}
                    <Link to="/ta-fair/exhibition">
                        <Button
                            bgColor="orange.dark.default"
                            width={{ base: "80vw", lg: "500px" }}
                            fontSize="lg"
                            p={8}
                            m={8}
                            rightIcon={<ArrowForwardIcon />}
                        >
                            Lanjutkan Ke Galeri
                        </Button>
                    </Link>
                </Box>
            </FadeInSection>
        </Box>
    )
}

export default TAFairAboutContent;