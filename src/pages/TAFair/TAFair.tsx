import React from "react";
import { useState, useEffect, FormEventHandler } from "react";
import {
  Box,
  Image,
  Flex,
  Heading,
  Text,
  FormControl,
  FormLabel,
  Input,
  Select,
  Button,
  useToast,
} from "@chakra-ui/react";
import { useNavigate } from "react-router";
import { motion } from "framer-motion";
import Layout from "../../components/layout/Layout";
import bg1 from "../../assets/images/tafair/landing/bg1.jpg";
import bg3 from "../../assets/images/tafair/landing/bg3.jpg";
import bg4 from "../../assets/images/tafair/landing/bg4.jpg";
import bg5 from "../../assets/images/tafair/landing/bg5.jpg";
import bg6 from "../../assets/images/tafair/landing/bg6.jpg";
import bg7 from "../../assets/images/tafair/landing/bg7.jpg";
import bg8 from "../../assets/images/tafair/landing/bg8.jpg";
import bg9 from "../../assets/images/tafair/landing/bg9.jpg";
import bg10 from "../../assets/images/tafair/landing/bg10.jpg";
import bg11 from "../../assets/images/tafair/landing/bg11.jpg";
import bg12 from "../../assets/images/tafair/landing/bg12.jpg";
import bg13 from "../../assets/images/tafair/landing/bg13.jpg";
import bg14 from "../../assets/images/tafair/landing/bg14.jpg";
import bg15 from "../../assets/images/tafair/landing/bg15.jpg";
import bg16 from "../../assets/images/tafair/landing/bg16.jpg";
import bg17 from "../../assets/images/tafair/landing/bg17.jpg";
import bg18 from "../../assets/images/tafair/landing/bg18.jpg";
import bg19 from "../../assets/images/tafair/landing/bg19.jpg";
import bg20 from "../../assets/images/tafair/landing/bg20.jpg";
import GoogleLogin from "../../components/login/GoogleLogin";
import { useAuth } from "../../hooks/useAuth";
import axios from "axios";

const BACKEND_URL = import.meta.env.VITE_BACKEND_URL;

function TAFair() {
  const { login, user, token, revalidate } = useAuth();
  const [showForm, setShowForm] = useState(false);

  const [name, setName] = useState("");
  const [institusi, setInstitusi] = useState("");
  const [jurusan, setJurusan] = useState("");

  const toast = useToast();

  const navigate = useNavigate();

  const handleSubmit: FormEventHandler = async (e) => {
    try {
      e.preventDefault();

      const data = {
        nama: name,
        institusi,
        fakultas: jurusan,
      };

      await axios.patch(`${BACKEND_URL}/account`, data, {
        headers: {
          authorization: `Bearer ${token}`,
        },
      });

      await revalidate();

      toast({
        title: "Sukses",
        description: "Berhasil menambahkan data",
        status: "success",
        isClosable: true,
        position: "top",
      });
    } catch (err) {
      toast({
        title: "Gagal Mendaftarkan",
        description: "Gagal mendaftarkan buku tamu",
        status: "error",
        isClosable: true,
        position: "top",
      });
    }
  };

  const container = {
    hidden: { opacity: 0 },
    show: {
      opacity: 1,
      transition: {
        staggerChildren: 0.5,
      },
    },
  };

  const onLogin = (data: CredentialResponse) => {
    if (data.credential) login(data.credential);
  };

  useEffect(() => {
    if (!user) {
      return;
    }

    if (user.is_new) {
      setShowForm(true);
    } else {
      navigate("/ta-fair/about");
    }
  }, [user]);

  const item = {
    hidden: { opacity: 0 },
    show: { opacity: 1 },
  };

  const [blur, setBlur] = useState(false);
  setTimeout(() => {
    setBlur(true);
  }, 5000);

  return (
    <Layout navbarVar="krem transparent" noFooter title="TA Fair">
      <motion.div variants={container} initial="hidden" animate="show">
        {/* BACKGROUND */}
        <Flex
          width="full"
          height="100vh"
          overflow="hidden"
          position="absolute"
          top="0px"
          bgColor="black"
          filter={blur ? "blur(4px)" : "none"}
        >
          <motion.div variants={item}>
            <Image alt=""
              src={bg1}
              width={{ base: "35vw", lg: "20vw" }}
              position="absolute"
              left="47.33"
              top="99"
              transform={"rotate(15.24deg) translate(-20%, 0)"}
            />
            <Image alt=""
              src={bg9}
              width="20vw"
              position="absolute"
              left="882"
              top="99"
              transform={"rotate(-9deg) translate(-20%, 0)"}
            />
          </motion.div>
          <motion.div variants={item}>
            <Image alt=""
              src={bg6}
              width={{ base: "35vw", lg: "15vw" }}
              position="absolute"
              right="0"
              bottom="0"
              transform={"rotate(7.98deg)"}
            />
            <Image alt=""
              src={bg4}
              width={{ base: "35vw", lg: "20vw" }}
              position="absolute"
              right="0"
              top="100"
              transform={"rotate(7.63deg)"}
            />
            <Image alt=""
              src={bg5}
              width={{ base: "35vw", lg: "20vw" }}
              position="absolute"
              left="0"
              bottom="0"
              transform={"rotate(8.15deg)"}
            />
          </motion.div>
          <motion.div variants={item}>
            <Image alt=""
              src={bg3}
              width={{ base: "35vw", lg: "20vw" }}
              position="absolute"
              left="250"
              bottom="0"
              transform={"rotate(1.91deg)"}
            />
          </motion.div>
          <motion.div variants={item}>
            <Image alt=""
              src={bg7}
              width={{ base: "35vw", lg: "20vw" }}
              position="absolute"
              right="350"
              top="80"
              transform={"rotate(-0.56deg)"}
            />
          </motion.div>
          <motion.div variants={item}>
            <Image alt=""
              src={bg8}
              width={{ base: "35vw", lg: "20vw" }}
              position="absolute"
              right="200"
              top="80"
              transform={"rotate(-5deg)"}
            />
          </motion.div>
          <motion.div variants={item}>
            <Image alt=""
              src={bg11}
              width={{ base: "35vw", lg: "20vw" }}
              position="absolute"
              right="300"
              bottom="0"
              transform={"rotate(5deg)"}
            />
            <Image alt=""
              src={bg10}
              width={{ base: "35vw", lg: "20vw" }}
              position="absolute"
              left="200"
              top="20"
              transform={"rotate(-7deg)"}
            />
          </motion.div>
          <motion.div variants={item}>
            <Image alt=""
              src={bg12}
              width={{ base: "35vw", lg: "20vw" }}
              position="absolute"
              right="500"
              top="-10"
              transform={"rotate(-10deg)"}
            />
            <Image alt=""
              src={bg13}
              width={{ base: "35vw", lg: "20vw" }}
              position="absolute"
              left="600"
              bottom="-20"
              transform={"rotate(4deg)"}
            />
          </motion.div>
          <motion.div variants={item}>
            <Image alt=""
              src={bg14}
              width={{ base: "35vw", lg: "18vw" }}
              position="absolute"
              right="0"
              top="300"
              transform={"rotate(-15deg)"}
            />
            <Image alt=""
              src={bg15}
              width={{ base: "35vw", lg: "20vw" }}
              position="absolute"
              left="400"
              top="-5"
              transform={"rotate(8deg)"}
            />
          </motion.div>
          <motion.div variants={item}>
            <Image alt=""
              src={bg16}
              width={{ base: "35vw", lg: "18vw" }}
              position="absolute"
              right="500"
              bottom="-2"
              transform={"rotate(-8deg)"}
            />
            <Image alt=""
              src={bg17}
              width={{ base: "35vw", lg: "20vw" }}
              position="absolute"
              left="580"
              top="-2"
              transform={"rotate(5deg)"}
            />
            <Image alt=""
              src={bg18}
              width={{ base: "35vw", lg: "20vw" }}
              position="absolute"
              right="200"
              top="-2"
              transform={"rotate(2deg)"}
            />
          </motion.div>
          <motion.div variants={item}>
            <Image alt=""
              src={bg19}
              width={{ base: "35vw", lg: "25vw" }}
              position="absolute"
              right="800"
              bottom="100"
              transform={"rotate(-8deg)"}
            />
            <Image alt=""
              src={bg20}
              width={{ base: "35vw", lg: "20vw" }}
              position="absolute"
              right="300"
              top="150"
              transform={"rotate(-2deg)"}
            />
          </motion.div>
        </Flex>
        <motion.div variants={item}>
          <Flex
            width="full"
            height="100vh"
            position="absolute"
            top="0px"
            justifyContent="center"
            alignItems="center"
          >
            <Flex
              flexDirection="column"
              mx={{ base: "10vw", lg: "30vw" }}
              alignItems="center"
              bgColor="krem.2"
              padding="6"
              borderRadius="30px"
              boxShadow="0px 0px 1px rgba(0, 0, 0, 0.1), 0px 4px 4px rgba(0, 0, 0, 0.05)"
            >
              <Text fontWeight="700" fontSize={{ base: "18px", lg: "20px" }}>
                Hai! Selamat datang di
              </Text>
              <Heading textShadow="0px 4px 3px rgba(0, 0, 0, 0.25)">
                PAMERAN TUGAS AKHIR
              </Heading>
              <Text fontWeight="700" fontSize={{ base: "20px", lg: "24px" }}>
                Parade Wisuda Juli ITB 2022
              </Text>
              <Box mt={5} display={showForm ? "none" : "box"}>
                <GoogleLogin onLogin={onLogin} />
              </Box>
              <Box
                as="form"
                width={{ base: "full", lg: "800px" }}
                my={3}
                onSubmit={handleSubmit}
                display={showForm ? "box" : "none"}
              >
                <FormControl>
                  <FormLabel fontWeight="bold" my="3">
                    Nama
                  </FormLabel>
                  <Input
                    type="text"
                    id="nama"
                    value={name}
                    onChange={(e) => setName(e.target.value)}
                    placeholder="Nama Kamu"
                    width="full"
                    required
                    autoFocus
                  />
                </FormControl>
                <FormControl>
                  <FormLabel fontWeight="bold" my="3">
                    Institusi
                  </FormLabel>
                  <Input
                    type="text"
                    id="institusi"
                    value={institusi}
                    onChange={(e) => setInstitusi(e.target.value)}
                    name="institusi"
                    placeholder="Institut Teknologi Bandung"
                    width="full"
                    required
                    autoFocus
                  />
                </FormControl>
                <FormControl>
                  <FormLabel fontWeight="bold" my="3">
                    Jurusan
                  </FormLabel>
                  <Select
                    id="jurusan"
                    name="jurusan"
                    value={jurusan}
                    onChange={(e) => setJurusan(e.target.value)}
                    placeholder="Jurusan"
                    width="full"
                    required
                    autoFocus
                  >
                    <option value="NonITB">Non-ITB</option>
                    <option value="Teknik Dirgantara">
                      AE (Teknik Dirgantara)
                    </option>
                    <option value="Aktuaria">AK (Aktuaria)</option>
                    <option value="Arsitektur">AR (Arsitektur)</option>
                    <option value="Astronomi">AS (Astronomi)</option>
                    <option value="Rekayasa Pertanian">
                      BA (Rekayasa Pertanian)
                    </option>
                    <option value="Rekayasa Hayati">
                      BE (Rekayasa Hayati)
                    </option>
                    <option value="Biologi">BI (Biologi)</option>
                    <option value="Mikrobiologi">BM (Mikrobiologi)</option>
                    <option value="Teknologi Pascapanen">
                      BP (Teknologi Pascapanen)
                    </option>
                    <option value="Rekayasa Kehutanan">
                      BW (Rekayasa Kehutanan)
                    </option>
                    <option value="Desain Interior">
                      DI (Desain Interior)
                    </option>
                    <option value="Desain Komunikasi Visual">
                      DKV (Desain Komunikasi Visual)
                    </option>
                    <option value="Desain Produk">DP (Desain Produk)</option>
                    <option value="Teknik Biomedis">
                      EB (Teknik Biomedis)
                    </option>
                    <option value="Teknik Elektro">EL (Teknik Elektro)</option>
                    <option value="Teknik Tenaga Listrik">
                      EP (Teknik Tenaga Listrik)
                    </option>
                    <option value="Teknik Telekomunikasi">
                      ET (Teknik Telekomunikasi)
                    </option>
                    <option value="Sains dan Teknologi Farmasi">
                      FA (Sains dan Teknologi Farmasi)
                    </option>
                    <option value="Fisika">FI (Fisika)</option>
                    <option value="Farmasi Klinik dan Komunitas">
                      FKK (Farmasi Klinik dan Komunitas)
                    </option>
                    <option value="Teknik Geodesi dan Geomatika">
                      GD (Teknik Geodesi dan Geomatika)
                    </option>
                    <option value="Teknik Geologi">GL (Teknik Geologi)</option>
                    <option value="Teknik Informatika">
                      IF (Teknik Informatika)
                    </option>
                    <option value="Rekayasa Infrastruktur Lingkungan">
                      IL (Rekayasa Infrastruktur Lingkungan)
                    </option>
                    <option value="Kimia">KI (Kimia)</option>
                    <option value="Teknik Kelautan">
                      KL (Teknik Kelautan)
                    </option>
                    <option value="Kriya">KR (Kriya)</option>
                    <option value="Matematika">MA (Matematika)</option>
                    <option value="Manajemen">MB (Manajemen)</option>
                    <option value="Meteorologi">ME (Meteorologi)</option>
                    <option value="Teknik Metalurgi">
                      MG (Teknik Metalurgi)
                    </option>
                    <option value="Kewirausahaan">MK (Kewirausahaan)</option>
                    <option value="Manajemen Rekayasa">
                      MR (Manajemen Rekayasa)
                    </option>
                    <option value="Teknik Mesin">MS (Teknik Mesin)</option>
                    <option value="Teknik Material">
                      MT (Teknik Material)
                    </option>
                    <option value="Oseanografi">OS (Oseanografi)</option>
                    <option value="Teknik Pangan">PG (Teknik Pangan)</option>
                    <option value="Perencanaan Wilayah dan Kota">
                      PWK (Perencanaan Wilayah dan Kota)
                    </option>
                    <option value="Teknik dan Pengelolaan Sumber Daya Air">
                      SA (Teknik dan Pengelolaan Sumber Daya Air)
                    </option>
                    <option value="Teknik Sipil">SI (Teknik Sipil)</option>
                    <option value="Seni Rupa">SR (Seni Rupa)</option>
                    <option value="Sistem dan Teknologi Informasi">
                      STI (Sistem dan Teknologi Informasi)
                    </option>
                    <option value="Teknik Pertambangan">
                      TA (Teknik Pertambangan)
                    </option>
                    <option value="Teknik Bioenergi dan Kemurgi">
                      TB (Teknik Bioenergi dan Kemurgi)
                    </option>
                    <option value="Teknik Fisika">TF (Teknik Fisika)</option>
                    <option value="Teknik Geofisika">
                      TG (Teknik Geofisika)
                    </option>
                    <option value="Teknik Industri">
                      TI (Teknik Industri)
                    </option>
                    <option value="Teknik Kimia">TK (Teknik Kimia)</option>
                    <option value="Teknik Lingkungan">
                      TL (Teknik Lingkungan)
                    </option>
                    <option value="Teknik Perminyakan">
                      TM (Teknik Perminyakan)
                    </option>
                  </Select>
                </FormControl>
                <Box width="full" textAlign="center" my={3}>
                  <Button
                    type="submit"
                    bgColor="blue.normal.default"
                    width={{ base: "full", lg: "600px" }}
                  >
                    Mulai Perjalananmu
                  </Button>
                </Box>
              </Box>
            </Flex>
          </Flex>
        </motion.div>
      </motion.div>
    </Layout>
  );
}

export default TAFair;
