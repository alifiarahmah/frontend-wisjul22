import React from "react";
import {
    Box,
    Image,
    Flex,
    Heading,
    Text,
    Divider,
    OrderedList,
    ListItem,
} from "@chakra-ui/react";
import { Link } from "react-router-dom";
import Layout from "../../components/layout/Layout";
import tulipMerah from "../../assets/images/tafair/Tulip merah.png";
import tulipKuning from "../../assets/images/tafair/tulip kuning.png";
import liquid from "../../assets/images/tafair/liquid2.png";
import ta_data from "../../assets/json/tafair-offline.json";
import api from "../../assets/images/tafair/api2.png";

function TAFairMain() {
    return (
        <Layout navbarVar="blue" bg="darkgreen">
            <Image alt="" src={liquid} pos="absolute" bottom="0" width="full" />
            <Box mx="5vw" height="full" pos="relative" pb="30px">
                <Flex
                    flexDir={{ base: "column", md: "row" }}
                    justifyContent="center"
                    alignItems={{ base: "center", md: "normal" }}
                    textColor="krem.2"
                    pt="30px"
                    pb="10px"
                    px="3vw"
                >
                    <Heading
                        fontWeight="400"
                        fontSize={{ base: "7vw", md: "3.5vw" }}
                        textAlign={{ base: "center", lg: "left" }}
                        mt="30"
                        pr={{ base: "0", md: "8vw" }}
                    >
                        SELAMAT DATANG DI PAMERAN TUGAS AKHIR
                    </Heading>
                    <Flex
                        flexDir="row"
                        justifyContent={{ base: "center", md: "flex-end" }}
                        width={{ base: "200px", md: "240px" }}
                        textAlign="center"
                        fontSize={{ base: "18px", md: "20" }}
                        fontWeight="500"
                    >
                        <Link to="/ta-fair/about">
                            <Flex
                                width={{ base: "100px", md: "120px" }}
                                flexDir="column"
                            >
                                <Image alt=""
                                    src={tulipKuning}
                                    transform="translateX(5%)"
                                />
                                <Text wordBreak="normal">
                                    Tentang <br /> Pameran
                                </Text>
                            </Flex>
                        </Link>
                        <Link to="/ta-fair/exhibition">
                            <Flex
                                width={{ base: "100px", md: "120px" }}
                                flexDir="column"
                            >
                                <Image alt=""
                                    src={tulipMerah}
                                    transform="translateX(5%)"
                                />
                                <Text wordBreak="normal">
                                    Galeri <br /> Pameran
                                </Text>
                            </Flex>
                        </Link>
                    </Flex>
                </Flex>
                <Divider border="4px" borderColor="krem.2" />
                <Box my={10} px={{ base: 5, lg: 10 }}>
                    <Image alt=""
                        src="https://drive.google.com/uc?id=1fbtcfl4H2lXQRwmqEwGagw9okrdwzCmz"
                        width="100%"
                    />
                    <Box bg="bg.light" p={10} borderRadius={10} my={10}>
                        <Heading textAlign="center" my={5}>
                            Informasi Denah
                        </Heading>
                        <OrderedList spacing={3}>
                            {ta_data.map((item, index) => (
                                <ListItem key={index}>
                                    <Text
                                        _hover={{
                                            textDecoration: "underline",
                                        }}
                                    >
                                        <Link
                                            to={
                                                item.id
                                                    ? `/ta-fair/tugasakhir/${item.id}`
                                                    : "#"
                                            }
                                        >
                                            {item.Nama}
                                        </Link>
                                    </Text>
                                </ListItem>
                            ))}
                        </OrderedList>
                    </Box>
                </Box>
            </Box>
            {/* <Box pos="relative">
                 <Flex>
                    <Image alt="" src={api} pos="absolute" top="0" />
                    <Image alt="" src={api} pos="absolute" right="0" top="0"transform="scaleX(-1)" />
                </Flex>
            </Box> */}
        </Layout>
    );
}

export default TAFairMain;
