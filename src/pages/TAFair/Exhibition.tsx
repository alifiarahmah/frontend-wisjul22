import React, { useState } from 'react';
import { Box, Flex, Heading, Text, InputGroup, Input, InputLeftElement, Select } from '@chakra-ui/react';
import { Link } from 'react-router-dom';
import { SearchIcon } from '@chakra-ui/icons';
import Layout from '../../components/layout/Layout';
import fakultas from '../../assets/json/fakultas.json';
import useFetch from '../../hooks/useFetch';
import Loading from '../../components/Loading';
import NotFound from '../../components/NotFound';

function Exhibition(){
    const [query, setQuery] = useState("");
    const [fak, setFak] = useState("");
    const [jurusan, setJurusan] = useState("");
    const urlPath = `${import.meta.env.VITE_BACKEND_URL}/tafair`
    const { data, isLoading, error } = useFetch(urlPath, []);
    const generateFak = (currJurusan: string) => {
        let currFak = ""
        fakultas.map((f) => {
            if (f.himpunan.some((h) => {
                return (
                    h.jurusan.some((k) => {
                        return (
                            k == currJurusan
                        )
                    })
                )
            })) {
                currFak = f.singkatan
            }
        })
        return (currFak)
    }
    let filteredData = (data as any).data?.filter((datum: any) =>
        (datum.wisudawan.some((w: any) =>
            w.nama.toLowerCase().includes(query.toLowerCase())
        )) && (datum.nama_jurusan == jurusan || jurusan =="") && (generateFak(datum.nama_jurusan) == fak || fak == "")
    )
    return(
        <>
        {isLoading ?
            <Layout navbarVar="none" bg="darkblue">
                <Loading />
            </Layout> :
        error && !data ? 
            <NotFound /> :
        data ? 
            <Layout navbarVar="none" bg="darkblue" title="TA Fair Exhibition">
                <Box px="5vw" py="100px" display="block">
                    <Link to ="/ta-fair/exhibition" onClick={(e) => {setFak(""); setJurusan("")}}>
                        <Heading
                            fontSize="5xl"
                            textAlign={{ base: "center", md: "left" }}
                            textColor="krem.1"
                        >
                            EXHIBITION GALLERY
                        </Heading>
                    </Link>
                    <Flex
                        direction={{ base: "column", md: "row" }}
                        justifyContent="space-between"
                        alignItems="center"
                        mt={3}
                    >
                        <Flex flexGrow="1">
                            <InputGroup>
                                <InputLeftElement
                                    children={<SearchIcon />}
                                    mx={2}
                                />
                                <Input
                                    type="text"
                                    placeholder="Cari Wisudawan"
                                    bg="bg.light"
                                    color="bg.dark"
                                    borderRadius="full"
                                    value={query}
                                    onChange={(e) => setQuery(e.target.value)}
                                />
                            </InputGroup>
                        </Flex>
                        <Flex pl="2vw" display={{base:"none", md:"flex"}}>
                            <Select variant="filled" placeholder="Fakultas" bg="bg.light" color="bg.dark" borderRadius="full" onChange={(e) => {setFak(e.target.value); setJurusan("")}} mx="1vw" width="10vw" autoFocus>
                                {
                                fakultas.map((f) => {
                                    return (
                                        <option value={f.singkatan}>
                                            {f.singkatan}
                                        </option>
                                    )
                                }) 
                                }
                            </Select>
                            <Select variant="filled" placeholder="Jurusan" bg="bg.light" color="bg.dark" borderRadius="full"  onChange={(e) => setJurusan(e.target.value)} width="10vw" disabled={fak =="" ? true: false} autoFocus>
                                {
                                fakultas.map((f) => {
                                    return (
                                    f.singkatan == fak ?
                                    f.himpunan.map((j) => {
                                        return (
                                            j.jurusan.map((k) => {
                                                return(
                                                    <option value={k}>{k}</option>
                                                )
                                            })
                                        )
                                    })
                                    :
                                    null
                                    )
                                }) 
                                }
                            </Select>
                        </Flex>
                    </Flex>
                    <Flex
                        mt={10}
                        flexWrap="wrap"
                        gap={3}
                        alignItems="center"
                        justifyContent={{base:"center", lg:"center"}}
                    >
                        {   
                            filteredData?.map((datum: any, i: any) => {
                                return (
                                    <Link to={"/ta-fair/tugasakhir/" + datum.id_ta}>
                                        <Box
                                            borderRadius="3xl"
                                            bg="bg.light"
                                            boxShadow="6px 6px 0 #FFFAE6"
                                            height="500px"
                                            width={{ base: "60vw", lg: "18vw" }}
                                            mx={{base:"auto", md:"1.5vw"}}
                                            borderColor={(i % 12 == 0) || (i % 12 == 6) || (i % 12 == 9) ? "#7FCEAF" : (i % 12 == 1) || (i % 12 == 4) || (i % 12 == 10) ? "#EEB8E9" : (i % 12 == 2) || (i % 12 == 7) || (i % 12 == 8) ? "#FFE37F" : "#FF824D"}
                                            my={3}
                                            borderBottomWidth="6px"
                                            borderRightWidth="6px"
                                            key={i}
                                            _hover={{
                                                background: "krem.1",
                                            }}
                                        >
                                            <Box
                                                p={{ base: "5", lg: "2vw" }}
                                                textAlign="center"
                                            >
                                                <Box my="100px">
                                                    <Heading
                                                        fontSize="2xl"
                                                        noOfLines={4}
                                                    >
                                                        {datum.judulTA}
                                                    </Heading>
                                                </Box>
                                                {
                                                    datum.wisudawan?.map((w: any, i: any) => {
                                                        return (
                                                            <>
                                                                <Text
                                                                    fontSize="lg"
                                                                    fontWeight="500"
                                                                >
                                                                    {w.nama}
                                                                </Text>
                                                                <Text
                                                                    fontSize="lg"

                                                                >
                                                                    {w.nim}
                                                                </Text>
                                                            </>
                                                        )
                                                    })
                                                }
                                            </Box>
                                        </Box>
                                    </Link>
                                )}
                            )
                        }
                    </Flex>
                </Box>
            </Layout>:
            <Layout navbarVar="krem">
                <Heading textAlign="center">DATA TIDAK DITEMUKAN</Heading>
            </Layout>
        }
        </>
    )
}

export default Exhibition