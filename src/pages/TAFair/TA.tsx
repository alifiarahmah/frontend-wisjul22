import React, { useState, useEffect, FormEventHandler } from "react";
import { useNavigate, useParams } from "react-router";
import {
    Box,
    Flex,
    Heading,
    Text,
    FormControl,
    FormLabel,
    Input,
    Button,
    Image,
    Divider,
    Textarea,
    Checkbox,
    useToast,
    Center,
} from "@chakra-ui/react";
import Layout from "../../components/layout/Layout";
import liquid from "../../assets/images/tafair/liquid3.png";
import tulip from "../../assets/images/tafair/tulip kuning2.png";
import pansies from "../../assets/images/tafair/pansies kuning.png";
import logo from "../../assets/images/logo/logo.png";
import festive1 from "../../assets/images/tafair/festive 3.png";
import pansies2 from "../../assets/images/tafair/pansies kuning merah.png";
import confetti from "../../assets/images/tafair/WISJUL VISTOCK2.png";
import festive2 from "../../assets/images/tafair/festive2.png";
import { AiOutlineSend } from "react-icons/ai";
import useFetch from "../../hooks/useFetch";
import Loading from "../../components/Loading";
import NotFound from "../../components/NotFound";
import axios from "axios";
import { useAuth } from "../../hooks/useAuth";
import GoogleLogin from "../../components/login/GoogleLogin";

function TA() {
    const [comment, setComment] = useState("");
    const { login, token, revalidate, user } = useAuth();
    const [isAnon, setIsAnon] = useState(false);
    let { id } = useParams();
    const urlPath = `${import.meta.env.VITE_BACKEND_URL}/tafair/${id}`;
    const { data, isLoading, toggleRefetch } = useFetch(urlPath, []);
    const isNull = (d: any) => {
        if (d == null) {
            return "-";
        } else {
            return d;
        }
    };
    const navigate = useNavigate();
    const toast = useToast();

    const onLogin = (data: CredentialResponse) => {
        if (data.credential) login(data.credential);
    };

    useEffect(() => {
        if(user?.is_new && token) {
            navigate("/ta-fair");
        }
    }, [user]);

    const handleSubmit: FormEventHandler = async (e) => {
        try {
            e.preventDefault();
            await revalidate();

            const data = {
                komentar: comment,
                is_anonim: isAnon,
            };

            await axios.post(
                `${import.meta.env.VITE_BACKEND_URL}/tafair/${id}/comment`,
                data,
                {
                    headers: {
                        authorization: `Bearer ${token}`,
                    },
                }
            );

            toast({
                title: "Komentar berhasil dikirim",
                status: "success",
                duration: 9000,
                isClosable: true,
                position: "top",
            });
            toggleRefetch();
        } catch (err) {
            if (axios.isAxiosError(err)) {
                if (err.response?.status === 400) {
                    toast({
                        title: "Kamu hanya bisa mengirim komentar 1 kali",
                        status: "error",
                        duration: 9000,
                        isClosable: true,
                        position: "top",
                    });
                } else if (err.response?.status === 401) {
                    toast({
                        title: "Kamu harus login terlebih dahulu",
                        status: "error",
                        duration: 9000,
                        isClosable: true,
                        position: "top",
                    });
                }
                return;
            }

            toast({
                title: "Komentar gagal dikirim",
                status: "error",
                duration: 9000,
                isClosable: true,
                position: "top",
            });
        }
    };

    if (isLoading) {
        return (
            <Layout>
                <Loading />
            </Layout>
        );
    }

    if (!data || !(data as any).data) {
        return <NotFound />;
    }

    const datum = (data as any).data;
    const komentar = (data as any).komentar;

    return (
        <Layout navbarVar="krem"  title={`${datum.judulTA} - TA Fair`}>
            <Flex pos="relative" mt="0" justifyContent="center">
                <Image alt=""
                    src={tulip}
                    pos="absolute"
                    left="0"
                    top="0"
                    transform={{
                        base: "translateY(-60px)",
                        md: "translateY(0)",
                    }}
                    maxWidth={{ base: "40%", md: "full" }}
                    zIndex="0"
                />
                <Image alt=""
                    src={pansies}
                    pos="absolute"
                    right="0"
                    top="0"
                    transform={{
                        base: "translateY(-60px)",
                        md: "translateY(0)",
                    }}
                    maxWidth={{ base: "30%", md: "full" }}
                    zIndex="0"
                />
                <Image alt=""
                    src={liquid}
                    width="full"
                    pos="absolute"
                    top={{ base: "0", md: "-10" }}
                    zIndex="0"
                />
                <Image alt="" src={festive1} pos="absolute" top="400" left="0" />
                <Image alt="" src={festive2} pos="absolute" right="0" bottom="0" />
                <Image alt="" src={pansies2} pos="absolute" right="0" bottom="600" />
                <Image alt="" src={confetti} pos="absolute" left="0" bottom="200" />
                <Flex
                    flexDir="column"
                    mt="200px"
                    mx="10vw"
                    pos="relative"
                    zIndex="10"
                    alignItems="center"
                >
                    <Heading
                        fontSize={{ base: "30px", md: "4vw" }}
                        px={{ base: "10vw", lg: "5vw" }}
                        textAlign="center"
                    >
                        {datum.judulTA}
                    </Heading>
                    <Box
                        my={10}
                        width={{ base: "80vw", md: "58vw" }}
                        height={{ base: "700px", lg: "35vw" }}
                        pos="relative"
                        borderRadius="20px"
                    >
                        <iframe
                            src={
                                `https://drive.google.com/file/d/` +
                                datum.abstrak
                                    .match(/id=(.*)/gm)[0]
                                    .replace("id=", "") +
                                `/preview`
                            }
                            width="100%"
                            height="100%"
                        ></iframe>
                    </Box>
                    {datum.is_porto_file ? (
                        <Box
                            my={10}
                            width={{ base: "80vw", md: "58vw" }}
                            height={{ base: "700px", lg: "35vw" }}
                            pos="relative"
                            borderRadius="20px"
                        >
                            {/* sorry kalo misal agak bad practice but at least it works */}
                            <iframe
                                src={
                                    `https://drive.google.com/file/d/` +
                                    datum.link_portofolio
                                        .match(/\/d\/(.*)\//gm)[0]
                                        .replace("/d/", "")
                                        .replace("/", "") +
                                    `/preview`
                                }
                                width="100%"
                                height="100%"
                            ></iframe>
                        </Box>
                    ) : datum.link_portofolio ? (
                        <Box
                            my={10}
                            width={{ base: "80vw", md: "58vw" }}
                            height={{ base: "700px", lg: "35vw" }}
                            pos="relative"
                            borderRadius="20px"
                            background="white"
                        >
                            <iframe
                                src={
                                    `https://drive.google.com/embeddedfolderview?id=` +
                                    datum.link_portofolio
                                        .match(/\/folders\/(.*)/gm)[0]
                                        .replace("/folders/", "")
                                        .replace("?usp=sharing", "") +
                                    `#list`
                                }
                                width="100%"
                                height="100%"
                            ></iframe>
                        </Box>
                    ) : null}
                    {/* <Box my={10} width={{ base: "80vw", md: "58vw" }} height={{ base: "700px", lg: "35vw" }} pos="relative" borderRadius="20px" backgroundColor="white">
                                                    <iframe 
                                                        src={`https://drive.google.com/embeddedfolderview?id=1eOO4-HwfMHEqPw1CpwV4-t7WHpk_Gg3H#list`} 
                                                        width="100%" height="100%">
                                                    </iframe>
                                                </Box> */}
                    <Flex
                        flexDir={{ base: "column", lg: "row" }}
                        justifyContent={{ base: "center", md: "space-between" }}
                        pos="relative"
                        my={10}
                    >
                        <Flex
                            p={10}
                            width={{ base: "80vw", lg: "40vw" }}
                            flexDir="column"
                            bgColor="krem.2"
                            borderRadius="20px"
                            m={2}
                        >
                            <Heading>Profil Wisudawan</Heading>
                            {datum.wisudawan?.map((w: any) => {
                                return (
                                    <>
                                        <Flex flexDir="row" alignItems="center">
                                            <Image alt=""
                                                src={logo}
                                                width={{
                                                    base: "20vw",
                                                    lg: "10vw",
                                                }}
                                                borderRadius="0"
                                                wordBreak="break-word"
                                            ></Image>
                                            <Box fontWeight="600" py={5}>
                                                <Text>{w.nama}</Text>
                                                <Text>
                                                    {datum.nama_jurusan +
                                                        " | " +
                                                        datum.singkatan_jurusan}
                                                </Text>
                                                <Text>Dosen pembimbing: </Text>
                                                <Text fontWeight="400">
                                                    {isNull(
                                                        datum.dosen_pembimbing
                                                    )}
                                                </Text>
                                                <Text>Motto Hidup :</Text>
                                                <Text fontWeight="400">
                                                    {isNull(datum.moto_hidup)}
                                                </Text>
                                            </Box>
                                        </Flex>
                                    </>
                                );
                            })}
                            <Text fontWeight="600">Cerita pengerjaan TA:</Text>
                            <Text whiteSpace="pre-wrap">
                                {isNull(datum.cerita_TA)}
                            </Text>
                        </Flex>
                        <Flex
                            p={10}
                            width={{ base: "80vw", lg: "40vw" }}
                            flexDir="column"
                            bgColor="krem.2"
                            borderRadius="20px"
                            m={2}
                        >
                            <Heading>Kisah Perkuliahan</Heading>
                            <Flex flexDir="row" alignItems="center">
                                <Text whiteSpace="pre-wrap">
                                    {isNull(datum.cerita_kuliah)}
                                </Text>
                            </Flex>
                        </Flex>
                    </Flex>
                    <Box
                        width="75vw"
                        bgColor="krem.2"
                        borderRadius="20px"
                        m={2}
                        p={10}
                    >
                        <Heading>Komentar</Heading>
                        {komentar.length > 0 ? (
                            komentar.map((comments: any, i: number) => {
                            return (
                                <>
                                    <Box my={5}>
                                        {i > 0 ? (
                                            <Divider
                                                margin={0}
                                                border="3px"
                                                borderColor="#BDBAAB"
                                            />
                                        ) : null}
                                        <Box my={3}>
                                            <Heading fontSize="xl" mt={5}>
                                                FROM {comments.nama_pengirim}
                                            </Heading>
                                            <Text>
                                                {comments.komentar}
                                            </Text>
                                        </Box>
                                    </Box>
                                </>
                            );
                            })
                        ):
                            <Text my={5} textAlign="center">Tidak ada komentar</Text>
                        }
                    </Box>
                    <Box
                        width="75vw"
                        bgColor="krem.2"
                        borderRadius="20px"
                        m={10}
                        p={10}
                    >
                        <Heading>Tambahkan Komentar</Heading>
                        <Box my={3}>
                            {token ? (
                                <form onSubmit={handleSubmit}>
                                    <Box
                                        borderRadius="20px"
                                        p={3}
                                        boxShadow="0px 0px 1px rgba(0, 0, 0, 0.15), 0px 4px 4px rgba(0, 0, 0, 0.08);"
                                    >
                                        <FormControl>
                                            <FormLabel fontWeight="bold" my={2}>
                                                Komentar (hanya bisa mengirim 1
                                                kali)
                                            </FormLabel>
                                            <Textarea
                                                variant="unstyled"
                                                id="nama"
                                                name="nama"
                                                placeholder="Masukkan komentar disini"
                                                width="full"
                                                value={comment}
                                                onChange={(e) =>
                                                    setComment(e.target.value)
                                                }
                                                required
                                            />
                                        </FormControl>
                                    </Box>
                                    <Checkbox
                                        my={3}
                                        isChecked={isAnon}
                                        onChange={() => setIsAnon(!isAnon)}
                                    >
                                        Komentar sebagai anonim
                                    </Checkbox>
                                    <Flex
                                        width="inherit"
                                        mt={10}
                                        justifyContent="center"
                                    >
                                        <Button
                                            type="submit"
                                            bgColor="orange.normal"
                                            mx="auto"
                                            rightIcon={<AiOutlineSend />}
                                            borderRadius="10px"
                                        >
                                            Kirim
                                        </Button>
                                    </Flex>
                                </form>
                            ) : (
                                <>
                                    <Box>
                                        <Text>
                                            Untuk menambahkan komentar, kamu
                                            harus login dulu.
                                        </Text>
                                    </Box>
                                    <Center my={3}>
                                        <GoogleLogin onLogin={onLogin} />
                                    </Center>
                                </>
                            )}
                        </Box>
                    </Box>
                </Flex>
            </Flex>
        </Layout>
    );
}

export default TA;
