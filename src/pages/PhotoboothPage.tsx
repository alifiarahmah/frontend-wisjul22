import { Box } from "@chakra-ui/react";
import Photobooth from "../components/photobooth/Photobooth";
import bg_himud from "../assets/images/bg_tile_lightgreen.jpg";
import Layout from "../components/layout/Layout";

export default function PhotoboothPage() {
  return (
    <Layout navbarVar="krem transparent"  title="Photobooth">
      <Box
        width="100vw"
        height="100vh"
        backgroundImage={`url(${bg_himud})`}
        backgroundSize="100vw auto"
        backgroundRepeat={"repeat"}
        overflow="hidden"
        zIndex={-100}
        position="absolute"
        top='0px'
      >
        <Box
          height='72px'
          position='relative'
          top=""
          width="100vw"
        >
        </Box>
        <Photobooth />
      </Box>
      </Layout>
  );
}
