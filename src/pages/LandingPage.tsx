import { Box } from "@chakra-ui/react";
import { useState, useEffect } from "react";
import Loading from "../components/Loading";


import Landing from "../components/landing/Landing";
import Layout from "../components/layout/Layout";

export default function LandingPage() {
  const [loading, setLoading] = useState(true);
  useEffect(() => {
    const loadData = async () => {
      await setTimeout(() => {
        setLoading(false);

      }
        , 1000);
    }
    loadData();
  }
    , []);

  if (loading) {
    return (
      <Layout>
        <Loading />
      </Layout>
    );
  }
  return (
    <Layout footerBg="dark">
      <Box backgroundImage={""} backgroundSize="100vw auto" backgroundRepeat={'repeat'} overflow="hidden">
        <Landing />
      </Box>
    </Layout>
  );
}
