import {
  Box,
  Container,
  Flex,
  Grid,
  Heading,
  Image,
  Text,
} from '@chakra-ui/react';
import { useParams } from 'react-router';
import vistocks from '../../assets/images/vistocks';
import Layout from '../../components/layout/Layout';
import useFetch from '../../hooks/useFetch';
import { IWOAStory } from '../../types/woa-types';
import parse, {
  DOMNode,
  domToReact,
  Element,
  HTMLReactParserOptions,
} from 'html-react-parser';
import SekeletonImg from '../../components/SekeletonImg';
import Loading from '../../components/Loading';
// import SekeletonImg from '../../components/SekeletonImg';

interface IFetchSuccess {
  status: string;
  data: IWOAStory;
}

export default function WOAById(): JSX.Element {
  const { id } = useParams();

  const {
    data: { data },
    error,
    isLoading,
  } = useFetch<IFetchSuccess>(`/story/${id}`, {
    status: '',
    data: {
      NIM_wisudawan: 0,
      html_story: '',
      thumbnail_path: null,
      judul: '',
      penulis: null,
      profile_pic: null,
    },
  });

  const options: HTMLReactParserOptions = {
    replace: (domNode: DOMNode) => {
      if (
        domNode instanceof Element &&
        domNode.type === 'tag' &&
        domNode.name === 'p'
      ) {
        return (
          <Text
            my="1rem"
            fontSize={{ base: 'md', lg: 'lg', xl: 'xl' }}
            {...domNode.attribs}
          >
            {domToReact(domNode.children)}
          </Text>
        );
      }

      if (
        domNode instanceof Element &&
        domNode.type === 'tag' &&
        domNode.name === 'b'
      ) {
        return (
          <Text
            my="1rem"
            fontWeight="bold"
            fontSize={{ base: 'md', lg: 'lg', xl: 'xl' }}
            {...domNode.attribs}
          >
            {domToReact(domNode.children)}
          </Text>
        );
      }

      if (
        domNode instanceof Element &&
        domNode.type === 'tag' &&
        domNode.name === 'i'
      ) {
        return (
          <Text
            my="1rem"
            fontStyle="italic"
            fontSize={{ base: 'md', lg: 'lg', xl: 'xl' }}
            {...domNode.attribs}
          >
            {domToReact(domNode.children)}
          </Text>
        );
      }

      if (
        domNode instanceof Element &&
        domNode.type === 'tag' &&
        domNode.name === 'img'
      ) {
        console.log(domNode);
        return (
          <SekeletonImg
            src={domNode.attribs.src}
            mx="auto"
            maxH="100vh"
            height="auto"
            my="1rem"
          />
        );
      }
    },
  };

  return (
    <Layout>
      {isLoading ? (
        <Loading />
      ) : error.isError ? (
        <Flex h="100vh" justifyContent="center" alignItems="center">
          <Heading fontSize="7xl">Terjadi Kesalahan</Heading>
        </Flex>
      ) : (
        <Box pos="relative" minH="100vh" overflow="hidden">
          {/* Ornamen Box */}
          <Box pos="absolute" left={0} right={0} top={0} bottom={0}>
            {/* Top Page */}
            <Image alt=""
              src={vistocks.tulip2}
              pos="absolute"
              top={{ base: '3vw', md: 0 }}
              left="-8vw"
              transform="rotate(156.57deg)"
              w="18.5vw"
            />
            <Image alt=""
              src={vistocks.sun4}
              pos="absolute"
              top={{ base: '-3vw', md: '-6vw' }}
              right="-8vw"
              transform="rotate(-26.06deg)"
              w="18.5vw"
            />
            <Image alt=""
              src={vistocks.liquid15B}
              width="100%"
              transform={{
                base: 'translateY(calc(-50% - 2vw))',
                md: 'translateY(calc(-50% - 5vw))',
              }}
            />

            {/* Bottom Page */}
            <Image alt=""
              src={vistocks.wisjulVistock3}
              pos="absolute"
              bottom="-6vw"
              left="-8vw"
              transform="rotate(163.53deg)"
              w="16.5vw"
            />
            <Image alt=""
              src={vistocks.wisjulVistock6}
              pos="absolute"
              bottom="-3vw"
              right="-4vw"
              w="14.5vw"
            />
          </Box>

          {/* Content Box */}
          <Container
            maxW="container.lg"
            pos="relative"
            as="article"
            px="2em"
            py={['2.5em', '4em', '5.5em', '7em']}
          >
            <Box as="header">
              <Heading
                as="h1"
                textAlign="center"
                size={{ base: '2xl', lg: '3xl' }}
              >
                {data.judul}
              </Heading>

              {/* <Box maxW="max-content" p={0} mt="1.5em" mx="auto">
              <SekeletonImg
                src="https://images.unsplash.com/photo-1657319484431-5c206bda688c?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80"
                alt="keterangan gambar"
                objectFit="cover"
                mx="auto"
                maxH="24rem"
              />
              <Text fontSize={{ base: 'xs', lg: 'sm', xl: 'md' }} mt="0.25em">
                Ditulis oleh Reini Wirahadikusumah
              </Text>
            </Box> */}
            </Box>

            <Box
              as="main"
              mt="1.5em"
              px={{ base: '0.5rem', lg: '1.5rem', xl: '3rem' }}
              // templateColumns={{
              //   base: 'auto',
              //   lg: 'minmax(0, 1fr) minmax(0, 0.45fr)',
              //   xl: 'minmax(0, 1fr) minmax(0, 0.35fr)',
              // }}
              // gap="3rem"
            >
              <Box as="section">
                <Text fontSize={{ base: 'xs', lg: 'sm', xl: 'md' }} mb="0.25em">
                  Ditulis oleh {data.penulis}
                </Text>

                {parse(data.html_story, options)}
                {/* <Text fontSize={{ base: 'md', lg: 'lg', xl: 'xl' }}>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam
                eu turpis molestie, dictum est a, mattis tellus. Sed dignissim,
                metus nec fringilla accumsan, risus sem sollicitudin lacus, ut
                interdum tellus elit sed risus. Maecenas eget condimentum velit,
                sit amet feugiat lectus. Class aptent taciti sociosqu ad litora
                torquent per conubia nostra, per inceptos himenaeos. Praesent
                auctor purus luctus enim egestas, ac scelerisque ante pulvinar.
                Donec ut rhoncus ex. Suspendisse ac rhoncus nisl, eu tempor
                urna. Curabitur vel bibendum lorem. Morbi convallis convallis
                diam sit amet
              </Text> */}
              </Box>

              {/* <Box
              as="section"
              display={{
                base: 'none',
                lg: 'block',
              }}
            >
              <Heading as="h5" size="md" fontWeight={700} fontFamily="body">
                Nama Penulis
              </Heading>
              <Text mt="0.875rem" fontSize={{ base: 'md', xl: 'lg' }}>
                Lorem ipsum dolor sit amet consectetur adipisicing elit.
                Delectus, consequuntur voluptatem. Est numquam, veritatis ex qui
                placeat similique voluptate iure itaque magni minima praesentium
                labore quod, vero aperiam.
              </Text>
            </Box> */}
            </Box>
          </Container>
        </Box>
      )}
    </Layout>
  );
}
