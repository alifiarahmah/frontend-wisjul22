import { Box, Flex, Heading, Image } from '@chakra-ui/react';
import parse, {
  DOMNode,
  domToReact,
  Element,
  HTMLReactParserOptions,
} from 'html-react-parser';

import Layout from '../../components/layout/Layout';
import Loading from '../../components/Loading';
import Card from '../../components/woa/Card';
import useFetch from '../../hooks/useFetch';

import vistocks from '../../assets/images/vistocks';
import logo from '../../assets/images/logo/logo_border.png';

import { IWOAStory } from '../../types/woa-types';

interface IFetchSuccess {
  status: string;
  data: IWOAStory[];
}

export default function WOAPage(): JSX.Element {
	const {
		data: { data },
		error,
		isLoading,
	} = useFetch<IFetchSuccess>("/story", {
		status: "",
		data: [],
	});

	const options: HTMLReactParserOptions = {
		replace: (domNode: DOMNode) => {
			if (
				domNode instanceof Element &&
				domNode.type === "tag" &&
				domNode.name === "p"
			) {
				return <>{domToReact(domNode.children)}</>;
			}
			return <></>;
		},
	};
	console.log(error);

	return (
		<Layout>
			{isLoading ? (
				<Loading />
			) : error.isError ? (
				<Flex h="100vh" justifyContent="center" alignItems="center">
					<Heading fontSize="7xl">Terjadi Kesalahan</Heading>
				</Flex>
			) : (
				<Box
					pos="relative"
					px={{ base: "2em", md: "5em" }}
					py={{ base: "2.75em", lg: "4em" }}
				>
					{/* Ornamen Box */}
					<Box
						pos="absolute"
						left={0}
						right={0}
						top={0}
						bottom={0}
						overflow="hidden"
					>
						<Image alt=""
							src={vistocks.bungaTulipBiru}
							pos="absolute"
							top={0}
							left="-8vw"
							transform="matrix(0.82, -0.57, -0.57, -0.82, 0, 0)"
							w="20vw"
						/>
						<Image alt=""
							src={vistocks.bintangBiruHijo}
							pos="absolute"
							top={"-4vw"}
							right="-8vw"
							w="18.5vw"
						/>
						<Image alt=""
							src={vistocks.liquid4}
							w="100%"
							transform={{
								base: "scaleY(-1) translateY(calc(50% + 1vw))",
								sm: "scaleY(-1) translateY(calc(50% + 3vw))",
							}}
						/>
					</Box>

					{/* Content */}
					<Box pos="relative">
						<Heading
							as="h1"
							fontSize={{
								base: "2rem",
								sm: "2.5rem",
								lg: "3rem",
								xl: "4rem",
							}}
							textAlign={{
								base: "center",
								md: "left",
							}}
							pl={{ lg: "0.75em" }}
						>
							Wall of Appreciation
						</Heading>

						<Flex
							mt="2.5em"
							justifyContent="center"
							gap="1rem"
							wrap="wrap"
						>
							{data.length > 0 ? (
								data.map((story) => (
									<Card
										key={story.NIM_wisudawan}
										id={story.NIM_wisudawan}
										title={story.judul}
										nimJurusan={story.NIM_wisudawan}
										body={parse(story.html_story, options)}
										imgSrc={
											story.profile_pic ||
											story.thumbnail_path ||
											logo
										}
									/>
								))
							) : (
								<Heading>Tidak ada data</Heading>
							)}
							{/* <Card
              id="1"
              title="Nama Wisudawan"
              nimJurusan="NIM/Jurusan"
              body="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vulputate libero et velit interdum, ac aliquet odio mattis."
              imgSrc="https://images.unsplash.com/photo-1657273359563-a140f1618269?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1887&q=80"
              imgAlt="Nama Wisudawan"
            />
            <Card
              id="2"
              title="Nama Wisudawan"
              nimJurusan="NIM/Jurusan"
              body="Lorem ipsum dolor sit amet, consectetur adipiscing elit."
              imgSrc="https://images.unsplash.com/photo-1657161707347-11ff4a4fb9ad?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=387&q=80"
              imgAlt="Nama Wisudawan"
            />
            {Array(9)
              .fill(0)
              .map((_, i) => (
                <Card
                  key={i}
                  id={String(i + 3)}
                  title="Nama Wisudawan"
                  nimJurusan="NIM/Jurusan"
                  body="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vulputate libero et velit interdum, ac aliquet odio mattis."
                  imgSrc="https://images.unsplash.com/photo-1657161707347-11ff4a4fb9ad?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=387&q=80"
                  imgAlt="Nama Wisudawan"
                />
              ))} */}
						</Flex>
					</Box>
				</Box>
			)}
		</Layout>
	);
}