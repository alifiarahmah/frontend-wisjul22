import {
  Box,
  Flex,
  Heading,
  Image,
  Text,
  OrderedList,
  ListItem,
  Center,
  Grid,
  SimpleGrid,
  HStack,
  Stack,
} from '@chakra-ui/react';
import Layout from '../components/layout/Layout';
import Api, { ApiProps } from '../components/about/Api';
import liquid2 from '../assets/images/about/Liquid 2.webp';
import organogram from '../assets/json/organogram.json';
import getRawDriveImg from '../lib/getRawDrive';

function AboutUs() {
  return (
    <Layout title="Tentang Kami">
      <Box
        height="full"
        width="full"
        bgImage={`url("https://static.paradewisudaitb.com/assets/images/bg_tile_krem.jpg")`}
        bgColor="krem.2"
        overflow={['hidden']}
      >
        <Image alt="" 
          src={liquid2}
          transform={{ base: 'scale(1.8, -1.8)', lg: 'scale(1.4, -1.4)' }}
          mt={{ base: 0, lg: -10 }}
          mb={{ base: -20, lg: '-25vh' }}
        />
        <Heading
          textAlign={['center']}
          fontFamily={["'Milk Carton'"]}
          my={10}
          fontSize={{ base: '5xl', lg: '6xl' }}
          textShadow="0px 4px 3px rgba(0, 0, 0, 0.25)"
        >
          TENTANG KAMI
        </Heading>

        <Flex flexDir="row" alignItems="center">
          <Image
            loading="lazy"
            boxSize="270"
            src="https://static.paradewisudaitb.com/assets/images/bintangbiru.png"
            alt="Bintang Biru"
            display={{ base: 'none', lg: 'block' }}
          />
          <Box ml={{ base: 20, lg: 0 }}>
            <Heading
              fontFamily={["'Milk Carton'"]}
              fontSize={{ base: '4xl', lg: '5xl' }}
              my={5}
              display="flex"
              alignItems="center"
              textShadow="0px 4px 3px rgba(0, 0, 0, 0.25)"
            >
              <Image
                mr={5}
                w={10}
                loading="lazy"
                src="https://static.paradewisudaitb.com/assets/images/bintangbiru.png"
                alt="Bintang Biru"
                display={{ base: 'inline-block', lg: 'none' }}
              />
              VISI
            </Heading>
            <Text
              fontSize={{ base: 20, lg: 24 }}
              marginRight="20"
              fontWeight="bold"
            >
              Parade Wisuda Juli sebagai sarana Apresiasi Wisudawan Juli dan
              wadah Integrasi Elemen KM ITB 2022
            </Text>
          </Box>
        </Flex>

        <Flex flexDir={'row'} my={10} mb={20}>
          <Box ml={{ base: 20, lg: 40 }} mr={{ base: 20, lg: 0 }}>
            <Heading
              textAlign={{ base: 'left', lg: 'right' }}
              fontFamily={["'Milk Carton'"]}
              fontSize={{ base: '4xl', lg: '5xl' }}
              my={5}
              textShadow="0px 4px 3px rgba(0, 0, 0, 0.25)"
            >
              <Image
                mr={5}
                w={10}
                loading="lazy"
                src="https://static.paradewisudaitb.com/assets/images/bintangkuning.png"
                alt="Bintang Biru"
                display={{ base: 'inline-block', lg: 'none' }}
              />
              MISI
            </Heading>
            <OrderedList fontSize={{ base: 20, lg: 24 }} fontWeight="bold">
              <ListItem>
                Menjadikan Perayaan Wisuda Juli 2022 sebagai sarana apresiasi
                kepada purna studi ITB atas keberhasilan menyelesaikan
                pendidikan dan tanggung jawab dari mahasiswa ITB;
              </ListItem>
              <ListItem>
                Menjadikan Perayaan Wisuda Juli 2022 yang mampu memberikan citra
                “Poros Pergerakan Intelektual” sebagai semangat visi yang
                dibawakan Ketua Kabinet KM ITB 2022/2023 kepada masyarakat luas;
              </ListItem>
              <ListItem>
                Menjadikan Perayaan Wisuda Juli 2022 yang memiliki sinergi antar
                lembaga dengan tetap mempertahankan ciri khas atau budaya
                lembaga masing-masing;
              </ListItem>
              <ListItem>
                Menjadikan Perayaan Wisuda Juli 2022 sebagai ruang menciptakan
                karya dan berkreasi bagi purna studi sebagai bentuk bakti karya
                terakhir sebelum menjadi seorang sarjana seutuhnya;
              </ListItem>
              <ListItem>
                Menjadikan Perayaan Wisuda Juli 2022 sebuah rangkaian kegiatan
                yang dapat memberikan kenangan terbaik kepada panitia, massa
                kampus, dan wisudawan; dan
              </ListItem>
              <ListItem>
                Menjadikan rangkaian kegiatan Perayaan Wisuda Juli 2022 yang
                berjalan efektif dan efisien.
              </ListItem>
            </OrderedList>
          </Box>
          <Image
            loading="lazy"
            boxSize="270"
            src="https://static.paradewisudaitb.com/assets/images/bintangkuning.png"
            alt="Bintang Kuning"
            alignContent={['center']}
            display={{ base: 'none', lg: 'block' }}
          />
        </Flex>

        <Box width="60%" mx="auto" my={20}>
          <Heading
            textAlign={['center']}
            fontFamily={'Milk Carton'}
            fontSize={{ base: '5xl', lg: '6xl' }}
            textShadow="0px 4px 3px rgba(0, 0, 0, 0.25)"
          >
            ORGANOGRAM
          </Heading>
          {organogram.map((bidang: any) => (
            <Box key={bidang.nama}>
              <Heading
                textAlign="center"
                fontFamily={'Milk Carton'}
                fontSize={{ base: '3xl', lg: '4xl' }}
                textShadow="0px 4px 3px rgba(0, 0, 0, 0.25)"
                my={5}
              >
                {bidang.nama}
              </Heading>
              {bidang.divisi.map((divisi: any) => (
                  <Flex direction={{ base: "column", md: "row" }} key={divisi.nama} gap={{ base: 0, lg: 10 }} alignItems="flex-start" justifyContent="center">
                    {divisi.orang.map((orang: any) => 
                        <Api
                        key={orang.jabatan}
                        nama={orang.nama}
                        jurusan={orang.jurusan}
                        jabatan={orang.jabatan}
                        img={orang.img}
                      />)}
                  </Flex>
              ))}
            </Box>
          ))}
        </Box>
      </Box>
    </Layout>
  );
}

export default AboutUs;
