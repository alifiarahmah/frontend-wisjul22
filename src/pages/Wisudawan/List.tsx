import {
  Image,
  Box,
  Flex,
  Heading,
  Text,
  Input,
  InputGroup,
  InputLeftElement,
  SimpleGrid,
  Center,
} from '@chakra-ui/react';
import { useState } from 'react';
import Layout from '../../components/layout/Layout';
import { SearchIcon } from '@chakra-ui/icons';
import { Link, useNavigate, useParams } from 'react-router-dom';
import useFetch from '../../hooks/useFetch';
import Loading from '../../components/Loading';
import NotFound from '../../components/NotFound';
import getRawDrive from '../../lib/getRawDrive';
import { MdOutlineChevronLeft } from 'react-icons/md';

function WisudawanList() {
  // navbar stuffs bcs aAA
  const { id } = useParams();
  const navigate = useNavigate();

  // fetch data
  const urlPath = `${import.meta.env.VITE_BACKEND_URL}/himpunan/${(
    id as String
  ).toUpperCase()}/wisudawan`;
  const himPath = `${import.meta.env.VITE_BACKEND_URL}/himpunan/${(
    id as String
  ).toUpperCase()}`;
  const { data, isLoading } = useFetch(urlPath, []);
  const { data: himdata } = useFetch(himPath, []);

  // search
  const [search, setSearch] = useState('');

  if (isLoading) {
    return (
      <Layout>
        <Loading />
      </Layout>
    );
  }

  if (!data || !(data as any).data) {
    return <NotFound />;
  }

  const wisudawan = (data as any).data;
  // filter wisudawan or all wisudawan if search is empty
  const filteredWisudawan =
    search.length > 0
      ? wisudawan.filter((wisudawan: any) =>
          wisudawan.nama.toLowerCase().includes(search.toLowerCase())
        )
      : wisudawan;

  return (
    <Layout>
      <Box px={{ base: 5, lg: 10 }}>
        {/* <FakUnitNavbar
					handleSection={handleSection}
				/> */}
        <Box py={5} p={{ base: 5, lg: 10 }}>
          <Flex
            mb={10}
            alignItems="center"
            onClick={() => navigate(-1)}
            _hover={{
              cursor: 'pointer',
            }}
          >
            <MdOutlineChevronLeft />
            <Text ml={3}>Kembali</Text>
          </Flex>
          <Heading fontSize="5xl" textAlign={{ base: 'center', lg: 'left' }}>
            Galeri Wisudawan
          </Heading>
          <Flex
            direction={{ base: 'column', lg: 'row' }}
            justifyContent="space-between"
            alignItems="center"
          >
            <Flex my={5} alignItems="center">
              <Box width="10%" maxH="10vh" mr={5}>
                <Image alt="" src={(himdata as any).data[0].logo_url} loading="lazy" />
              </Box>
              <Heading fontSize="2xl">{(himdata as any).data[0].nama}</Heading>
            </Flex>
            <Box>
              <InputGroup>
                <InputLeftElement children={<SearchIcon />} mx={2} />
                <Input
                  type="text"
                  placeholder="Cari Wisudawan"
                  bg="bg.light"
                  color="bg.dark"
                  borderRadius="full"
                  onChange={(e) => setSearch(e.target.value)}
                />
              </InputGroup>
            </Box>
          </Flex>
          {/* List Wisudawan pake grid */}
          <SimpleGrid
            my={10}
            columns={{ base: 2, md: 3, lg: 4 }}
            spacingX="auto"
            spacingY={5}
            justifyContent="center"
            alignItems="stretch"
          >
            {isLoading ? (
              <Loading />
            ) : (
              filteredWisudawan.map((w: any) => {
                return (
                  <Link to={`/wisudawan/${w.NIM}`} key={`${w.NIM}`}>
                    <Center>
                      <Box
                        borderRadius={{ base: 'xl', lg: '3xl' }}
                        bg="bg.light"
                        boxShadow="base"
                        width={{ base: '35vw', md: '20vw', lg: '17vw' }}
                        my={3}
                        h="100%"
                      >
                        <Box
                          bg="gray.400"
                          p={5}
                          borderTopRadius="inherit"
                          bgImage={`url(${
                            w.foto_url
                              ? getRawDrive(w.foto_url)
                              : 'https://static.paradewisudaitb.com/assets/images/logo/logo_border.png'
                          })`}
                          bgSize={w.foto_url ? 'cover' : 'contain'}
                          bgRepeat="no-repeat"
                          bgPos={'center'}
                          h={{ base: '45vw', md: '25vw', lg: '22vw' }}
                        ></Box>
                        <Box p={5}>
                          <Heading fontSize="xl" noOfLines={2}>
                            {w.nama}
                          </Heading>
                          <Text>
                            {w.NIM}/{w.wisudawan_to_jurusan.singkatan}
                          </Text>
                        </Box>
                      </Box>
                    </Center>
                  </Link>
                );
              })
            )}
          </SimpleGrid>
        </Box>
      </Box>
    </Layout>
  );
}

export default WisudawanList;
