import {
	Box,
	ButtonGroup,
} from "@chakra-ui/react";
import { useState } from "react";
import fakultas from "../../assets/json/fakultas.json";
import units from "../../assets/json/unit.json";
import Layout from "../../components/layout/Layout";
import FakUnitNavbar from "../../components/wisudawan/FakUnitNavbar";
import HimUnitButtons from "../../components/wisudawan/HimUnitButtons";
import FakUnitPage from "../../components/wisudawan/FakUnitPage";

function Wisudawan(props: any) {
	// fakultas if props.fak is not undefined
	const [viewMode, setViewMode] = useState("Fakultas");
	const [fak, setFak] = props.fak ?? useState(fakultas[0]); // fakultas, kategori unit (olahraga seni dll.)
	const [him, setHim] = props.him ?? useState(fak.himpunan[0]); // himpunan, unit
	const [kateg, setKateg] = props.kateg ?? useState(units[0]);
	const [unit, setUnit] = props.unit ?? useState(kateg.units[0]);

	const handleViewMode = (mode: string) => {
		setViewMode(mode);
		if (mode === "Fakultas") {
			setFak(fakultas[0]);
			setHim(fak.himpunan[0]);
		} else {
			setKateg(units[0]);
			setUnit(kateg.units[0]);
		}
	};

	const handleSection = (e: any) => {
		if (viewMode === "Fakultas") {
			setFak(e);
			setHim(e.himpunan[0]);
		} else {
			setKateg(e);
			setUnit(e.units[0]);
		}
	};

	const handleSelected = (e: any) => {
		if (viewMode === "Fakultas") {
			setHim(e);
		} else {
			setUnit(e);
		}
	};

	return (
		<Layout>
			<Box>
				<FakUnitNavbar
					viewMode={viewMode}
					fak={fak}
					kateg={kateg}
					handleViewMode={handleViewMode}
					handleSection={handleSection}
				/>

				{/* Content */}
				<Box>
					{/* himpunan */}
					<ButtonGroup
						m={{ base: 5, lg: 10 }}
						display="flex"
						justifyContent="center"
						flexWrap="wrap"
					>
						{viewMode === "Fakultas"
							? fak.himpunan.map((h: any) => {
									return (
										<HimUnitButtons
											key={h.singkatan}
											onClick={() => {
												handleSelected(h);
											}}
										>
											{h.singkatan}
										</HimUnitButtons>
									);
							  })
							: kateg.units.map((u: any) => {
									return (
										<HimUnitButtons
											key={u.singkatan}
											onClick={() => handleSelected(u)}
										>
											{u.singkatan}
										</HimUnitButtons>
									);
							  })}
					</ButtonGroup>
					<FakUnitPage
						singkatan={
							viewMode === "Fakultas"
								? him.singkatan.split(" ")[0].toUpperCase()
								: unit.singkatan
						}
						type={viewMode === "Fakultas" ? "himpunan" : "ukm"}
					/>
				</Box>
			</Box>
		</Layout>
	);
}

export default Wisudawan;
