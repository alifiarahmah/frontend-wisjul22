import {
  Box,
  Button,
  Center,
  Checkbox,
  Flex,
  FormControl,
  FormLabel,
  Grid,
  GridItem,
  Heading,
  HStack,
  Image,
  Stack,
  Text,
  Textarea,
  useToast,
} from '@chakra-ui/react';
import { FormEventHandler, useState } from 'react';
import { Link, useNavigate, useParams } from 'react-router-dom';
import fakultas from '../../assets/json/fakultas.json';
import units from '../../assets/json/unit.json';
import Layout from '../../components/layout/Layout';
import useGooglePrompt from '../../hooks/GooglePrompt';
import useFetch from '../../hooks/useFetch';
import NotFound from '../../components/NotFound';
import { useAuth } from '../../hooks/useAuth';
import axios from 'axios';
import GoogleLogin from '../../components/login/GoogleLogin';
import { AiOutlineSend } from 'react-icons/ai';
import Loading from '../../components/Loading';
import getRawDrive from '../../lib/getRawDrive';
import { ArrowLeftIcon, ChevronLeftIcon } from '@chakra-ui/icons';
import { MdArrowLeft, MdOutlineChevronLeft } from 'react-icons/md';

function WisudawanProfile() {
  const [viewMode, setViewMode] = useState('Fakultas'); // mode fakultas/unit-bso-km
  const [fak, setFak] = useState(fakultas[0]); // fakultas, kategori unit (olahraga seni dll.)
  const [kateg, setKateg] = useState(units[0]);

  // comment & google auth stuffs
  const [comment, setComment] = useState('');
  const { login, token, revalidate, user } = useAuth();
  const [isAnon, setIsAnon] = useState(false);
  const toast = useToast();
  const onLogin = (data: CredentialResponse) => {
    if (data.credential) login(data.credential);
  };
  const handleSubmit: FormEventHandler = async (e) => {
    try {
      e.preventDefault();
      await revalidate();

      const data = {
        komentar: comment,
        is_anonim: isAnon,
      };

      await axios.post(
        `${import.meta.env.VITE_BACKEND_URL}/wisudawan/${id}/comment`,
        data,
        {
          headers: {
            authorization: `Bearer ${token}`,
          },
        }
      );

      toast({
        title: 'Komentar berhasil dikirim',
        status: 'success',
        duration: 9000,
        isClosable: true,
        position: 'top',
      });
      toggleRefetch();
      setComment('');
    } catch (err) {
      if (axios.isAxiosError(err)) {
        if (err.response?.status === 400) {
          toast({
            title: 'Kamu hanya bisa mengirim komentar 1 kali',
            status: 'error',
            duration: 9000,
            isClosable: true,
            position: 'top',
          });
        } else if (err.response?.status === 401) {
          toast({
            title: 'Kamu harus login terlebih dahulu',
            status: 'error',
            duration: 9000,
            isClosable: true,
            position: 'top',
          });
        }
        return;
      }

      toast({
        title: 'Komentar gagal dikirim',
        status: 'error',
        duration: 9000,
        isClosable: true,
        position: 'top',
      });
    }
  };

  let navigate = useNavigate();
  const { id } = useParams();

  const urlPath = `${import.meta.env.VITE_BACKEND_URL}/wisudawan/${id}`;
  const { data, isLoading, toggleRefetch } = useFetch(urlPath, []);

  // const handleViewMode = (mode: string) => {
  //   setViewMode(mode);
  //   if (mode === 'Fakultas') {
  //     setFak(fakultas[0]);
  //   } else {
  //     setKateg(units[0]);
  //   }
  // };

  // const handleSection = (e: any) => {
  //   navigate('/wisudawan');
  // };

  useGooglePrompt({
    onLogin: (data) => {
      if (data.credential) {
        login(data.credential);
      }
    },
    showPrompt: true,
  });

  if (isLoading) {
    return (
      <Layout>
        <Loading />
      </Layout>
    );
  }

  if (!data || !(data as any).data) {
    return <NotFound />;
  }

  const wisudawan = (data as any).data.wisudawan;
  const komentar = (data as any).data.komentar;
  const prestasi = (data as any).data.prestasi;
  const jurusan = (data as any).data.jurusan;
  const organisasi = (data as any).data.organisasi;
  const kepanitiaan = (data as any).data.kepanitiaan;

  if (isLoading) {
    return (
      <Layout>
        <Loading />
      </Layout>
    );
  }

  if (!data || !(data as any).data) {
    return <NotFound />;
  }

  return (
    <Layout>
      {/* <FakUnitNavbar
        viewMode={viewMode}
        fak={fak}
        kateg={kateg}
        handleViewMode={handleViewMode}
        handleSection={handleSection}
        hideDD={true}
      /> */}

      <Box px={{ base: 10, lg: 20 }} py={{ base: 5, lg: 10 }}>
        <Flex
          alignItems="center"
          onClick={() => navigate(-1)}
          _hover={{
            cursor: 'pointer',
          }}
        >
          <MdOutlineChevronLeft />
          <Text ml={3}>Kembali ke daftar wisudawan</Text>
        </Flex>
        <HStack
          flexDirection={{ base: 'column-reverse', lg: 'row' }}
          justifyContent={{ base: 'center', lg: 'flex-start' }}
          alignItems={{ base: 'center', lg: 'flex-start' }}
          my={10}
          gap={10}
        >
          <Image
            display={wisudawan.foto_url ? 'block' : 'none'}
            maxH={{ base: '70vh', lg: '30vh' }}
            maxW={{ base: '70vw', lg: '100%' }}
            borderRadius="xl"
            boxShadow="lg"
            src={wisudawan.foto_url ? getRawDrive(wisudawan.foto_url) : ''}
            alt={wisudawan.nama}
          />
          <Box>
            <Heading
              as="h2"
              fontSize={{ base: '3xl', lg: '5xl' }}
              mb={3}
              textAlign={{ base: 'center', lg: 'left' }}
            >
              {wisudawan.nama}
            </Heading>
            <Heading
              as="h5"
              fontSize={{ base: 'md', lg: '3xl' }}
              textAlign={{ base: 'center', lg: 'left' }}
            >
              {wisudawan.NIM}/{jurusan.nama}
            </Heading>
          </Box>
        </HStack>

        <Box
          mt={10}
          display={
            organisasi.length > 0 || kepanitiaan.length > 0 ? 'block' : 'none'
          }
        >
          <Heading
            as="h3"
            fontSize="2xl"
            textAlign={{ base: 'center', lg: 'left' }}
            my={3}
          >
            Organisasi & Kepanitiaan
          </Heading>
          <Grid
            p={5}
            bg="bg.light"
            borderRadius="lg"
            boxShadow="lg"
            gridTemplateColumns={{
              base: '(2, 1fr)',
              lg: '1fr 1fr',
            }}
            gap={5}
          >
            {organisasi.map((org: any) => (
              <GridItem>
                <Heading fontSize="xl">{org.nama}</Heading>
              </GridItem>
            ))}
            {kepanitiaan.map((org: any) => (
              <GridItem>
                <Heading fontSize="xl">{org.nama_kepanitiaan}</Heading>
              </GridItem>
            ))}
          </Grid>
        </Box>

        <Box mt={10} display={prestasi.length > 0 ? 'block' : 'none'}>
          <Heading
            as="h3"
            fontSize="2xl"
            textAlign={{ base: 'center', lg: 'left' }}
            my={3}
          >
            Prestasi
          </Heading>
          <Grid
            p={5}
            bg="bg.light"
            borderRadius="lg"
            boxShadow="lg"
            gridTemplateColumns={{
              base: '(2, 1fr)',
              lg: '1fr 1fr',
            }}
            gap={5}
          >
            {prestasi.map((prest: any) => (
              <GridItem>
                <Heading fontSize="xl">{prest.nama_prestasi}</Heading>
                <Text fontSize="lg">
                  {new Date(prest.tanggal).getFullYear()}
                </Text>
              </GridItem>
            ))}
          </Grid>
        </Box>

        <Box mt={10}>
          <Heading
            as="h3"
            fontSize="2xl"
            textAlign={{ base: 'center', lg: 'left' }}
            my={3}
          >
            Pesan untuk Wisudawan
          </Heading>
          <Stack
            p={5}
            bg="bg.light"
            borderRadius="lg"
            gap={5}
            boxShadow="0px 0px 1px rgba(0, 0, 0, 0.15), 0px 4px 4px rgba(0, 0, 0, 0.08);"
          >
            {komentar.length > 0 ? (
              komentar.map((komen: any) => {
                return (
                  <Box key="">
                    <Heading fontSize="xl">{komen.nama_pengirim}</Heading>
                    <Text fontSize="lg">{komen.komentar}</Text>
                  </Box>
                );
              })
            ) : (
              <Text textAlign="center" as="i">
                Belum ada komentar untuk wisudawan ini
              </Text>
            )}
          </Stack>
        </Box>

        <Box mt={10}>
          <Heading
            as="h3"
            fontSize="2xl"
            textAlign={{ base: 'center', lg: 'left' }}
            my={3}
          >
            Kirim Pesan
          </Heading>
          {token ? (
            <form onSubmit={handleSubmit}>
              <Box
                borderRadius="lg"
                p={3}
                boxShadow="0px 0px 1px rgba(0, 0, 0, 0.15), 0px 4px 4px rgba(0, 0, 0, 0.08);"
                bg="bg.light"
              >
                <FormControl>
                  <FormLabel fontWeight="bold" my={2}>
                    Komentar (hanya bisa mengirim 1 kali)
                  </FormLabel>
                  <Textarea
                    variant="unstyled"
                    id="nama"
                    name="nama"
                    placeholder="Masukkan komentar disini"
                    width="full"
                    value={comment}
                    onChange={(e) => setComment(e.target.value)}
                    required
                  />
                </FormControl>
              </Box>
              <Checkbox
                my={3}
                isChecked={isAnon}
                onChange={() => setIsAnon(!isAnon)}
              >
                Komentar sebagai anonim
              </Checkbox>
              <Flex width="inherit" mt={10} justifyContent="center">
                <Button
                  type="submit"
                  bgColor="orange.normal"
                  mx="auto"
                  rightIcon={<AiOutlineSend />}
                  borderRadius="10px"
                >
                  Kirim
                </Button>
              </Flex>
            </form>
          ) : (
            <>
              <Box
                borderRadius="lg"
                p={3}
                boxShadow="0px 0px 1px rgba(0, 0, 0, 0.15), 0px 4px 4px rgba(0, 0, 0, 0.08);"
                bg="bg.light"
              >
                <Text fontSize="sm" textAlign="center">
                  Log in untuk menambahkan komentar
                </Text>
                <Center my={3}>
                  <GoogleLogin onLogin={onLogin} />
                </Center>
              </Box>
            </>
          )}
        </Box>
      </Box>
    </Layout>
  );
}

export default WisudawanProfile;
