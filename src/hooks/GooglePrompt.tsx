import { Box } from "@chakra-ui/react";
import { useContext, useEffect } from "react";
import GSIScriptContext from "../context/GSIScriptContext";

export interface GooglePromptProps {
  onLogin: (data: CredentialResponse) => void;
  showPrompt: boolean;
}

export default function useGooglePrompt(
  { onLogin, showPrompt }: GooglePromptProps = {
    onLogin: () => {},
    showPrompt: false,
  }
) {
  const isLoaded = useContext(GSIScriptContext);

  useEffect(() => {
    if (typeof window === "undefined" || !window.google || !showPrompt) {
      return;
    }

    try {
      window.google.accounts.id.initialize({
        client_id: import.meta.env.VITE_GSI_CLIENT as string,
        callback: onLogin,
      });

      window.google.accounts.id.prompt();
    } catch (err) {
      console.error(`Can't load google login button : ${err}`);
    }
  }, [showPrompt, isLoaded]);

  return;
}
