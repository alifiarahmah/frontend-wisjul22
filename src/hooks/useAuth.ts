import { useToast } from "@chakra-ui/react";
import axios from "axios";
import { useContext } from "react";
import AuthContext from "../context/AuthContext";

const BACKEND_URL = import.meta.env.VITE_BACKEND_URL;

async function loginRunner(
  token: string,
  setUser?: Function,
  setToken?: Function,
  onSuccess: (message: string) => void = () => {},
  onFailed: (conf: {
    message: string;
    sevirity: "warning" | "error";
  }) => void = () => {},
  showSuccess = true
) {
  try {
    const { data } = await axios.post(
      `${BACKEND_URL}/login`,
      {},
      {
        headers: {
          authorization: `Bearer ${token}`,
        },
      }
    );

    setUser && setUser(data.data);
    setToken && setToken(token);

    showSuccess && onSuccess("Anda berhasil login");
  } catch (err) {
    if (axios.isAxiosError(err)) {
      if (err.response?.status === 403) {
        onFailed({
          message: "Anda tidak bisa mengakses situs ini karena telah diblokir.",
          sevirity: "error",
        });
      } else {
        onFailed({
          message: "Sesi anda telah berakhir. Silahkan login",
          sevirity: "warning",
        });
      }
    }

    onFailed({
      message: "Terjadi kesalahan saat melakukan autentikasi",
      sevirity: "error",
    });
    throw err;
  }
}

export function useAuth() {
  const toast = useToast();
  const { token, user, destroyUser, setToken, setUser } =
    useContext(AuthContext);

  const onSuccess = (message: string) => {
    toast({
      title: "Login Sukses",
      description: message,
      status: "success",
      isClosable: true,
      position: "top",
    });
  };

  const onFailed = ({
    message,
    sevirity,
  }: {
    message: string;
    sevirity: "warning" | "error";
  }) => {
    toast({
      title: "Login Gagal",
      description: message,
      status: sevirity,
      isClosable: true,
      position: "top",
    });
  };

  const login = async (token: string) => {
    await loginRunner(token, setUser, setToken, onSuccess, onFailed);
  };

  const logout = async () => {
    destroyUser && destroyUser();
  };

  const revalidate = async () => {
    if (token)
      await loginRunner(
        token,
        setUser,
        setToken,
        () => {},
        ({ message, sevirity }) => {
          toast({
            title: sevirity === "warning" ? "Sesi Habis" : "Galat Server",
            description: message,
            status: sevirity,
            isClosable: true,
            position: "top",
          });
        },
        false
      );
  };

  return {
    token,
    user,
    login,
    logout,
    revalidate,
  };
}
