import { AxiosError, AxiosRequestConfig, AxiosResponse } from 'axios';
import { useEffect, useState } from 'react';

import api from '../lib/api';

export interface IError {
  isError: boolean;
  error?: AxiosError;
}

export interface IUseFetch<T = any> {
  data: T;
  isLoading: boolean;
  error: IError;
  toggleRefetch: () => void;
}

export default function useFetch<T = any, D = any>(
  config: AxiosRequestConfig<D> | string, // config bisa diisi url saja atau config request dari axios
  initialState: T, // untuk inisialisasi data awal (seperti pada useState)
  postpone = false // optional parameter untuk menunda fetching
): IUseFetch<T> {
  const [data, setData] = useState<T>(initialState);
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState<IError>({
    isError: false,
  });
  const [refetch, setRefetch] = useState<number>(Math.random());

  useEffect(() => {
    /** data fetching dilakukan apabila postpone bernilai false */
    if (!postpone) {
      setError({ isError: false });
      setIsLoading(true);

      api
        .request(
          typeof config === 'string'
            ? {
                method: 'GET',
                url: config,
              }
            : config
        )
        .then((response: AxiosResponse<T, D>) => {
          setData(response.data);
        })
        .catch((error: AxiosError) => {
          setError({
            isError: true,
            error,
          });
        })
        .finally(() => {
          setIsLoading(false);
        });
    }
  }, [config, refetch, postpone]);

  const toggleRefetch = () => {
    setRefetch(Math.random());
  };

  return {
    data,
    isLoading,
    error,
    toggleRefetch,
  };
}
