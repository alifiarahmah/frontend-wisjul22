export default interface IUser {
  name: string;
  email: string;
  faculty: string | null;
  institution: string | null;
  is_new: boolean;
}
