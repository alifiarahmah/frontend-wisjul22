export interface IWOAStory {
  NIM_wisudawan: number;
  html_story: string;
  thumbnail_path: string | null;
  judul: string;
  penulis: string | null;
  profile_pic: string | null;
}
