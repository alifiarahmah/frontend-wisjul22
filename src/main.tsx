import { ChakraProvider, ThemeProvider } from "@chakra-ui/react";
import React, { Component, Fragment } from "react";
import ReactDOM from "react-dom/client";
import App from "./App";
import { GSIScriptContextProvider } from "./context/GSIScriptContext";
import "./index.css";
import { theme } from "./theme";
import { BrowserRouter, Route, Link } from "react-router-dom";
import Kunsmatrix from "./pages/AboutUs";
import AboutUs from "./pages/AboutUs";
import { AuthProvider } from "./context/AuthContext";

ReactDOM.createRoot(document.getElementById("root")!).render(
  <React.StrictMode>
    <GSIScriptContextProvider>
      <AuthProvider>
        <ChakraProvider theme={theme}>
          <App />
        </ChakraProvider>
      </AuthProvider>
    </GSIScriptContextProvider>
  </React.StrictMode>
);
