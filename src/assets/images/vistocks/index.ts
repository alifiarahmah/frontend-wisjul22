import apiIkal1 from './api ikal 1.png';
import bintangBiruHijo from './Bintang biru hijo.png';
import bungaTulipBiru from './Bunga Tulip Biru.png';
import liquid4 from './liquid 4.png';
import liquid15B from './Liquid 15B.png';
import pansiesKuningMerah from './pansies kuning merah.png';
import sun4 from './sun 4.png';
import tulip2 from './Tulip 2.png';
import wisjulVistock2 from './WISJUL VISTOCK2.png';
import wisjulVistock3 from './WISJUL VISTOCK3.png';
import wisjulVistock5 from './WISJUL VISTOCK5.png';
import wisjulVistock6 from './WISJUL VISTOCK6.png';

export default {
  apiIkal1,
  bintangBiruHijo,
  bungaTulipBiru,
  liquid4,
  liquid15B,
  pansiesKuningMerah,
  sun4,
  tulip2,
  wisjulVistock2,
  wisjulVistock3,
  wisjulVistock5,
  wisjulVistock6,
};
