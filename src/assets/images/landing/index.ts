import p1 from "./medpar/p1.png";
import p2 from "./medpar/p2.png";
import p3 from "./medpar/p3.png";
import p4 from "./medpar/p4.png";
import p5 from "./medpar/p5.png";
import p6 from "./medpar/p6.png";
import p7 from "./medpar/p7.png";

const ASSETS_URL = "https://static.paradewisudaitb.com/assets/images/landing/";

const images = {
  ornamentTopmost: `${ASSETS_URL}Liquid6B 1.webp`,
  ornamentTopmid: `${ASSETS_URL}Liquid 3.webp`,
  ornamentBottommid: `${ASSETS_URL}swirl blue 2.webp`,
  ornamentBottommost: `${ASSETS_URL}Liquid 8 1.webp`,
  graphics1: `${ASSETS_URL}graphics1.webp`,
  festive1: `${ASSETS_URL}pngs/Festive1.webp`,
  festive2: `${ASSETS_URL}pngs/festive2.webp`,
  logoLockup: `${ASSETS_URL}pngs/logo_lockup.webp`,
  sun: `${ASSETS_URL}sun 5.webp`,
  stars1: `${ASSETS_URL}star 1 1.webp`,
  stars2: `${ASSETS_URL}star 2 1.webp`,
  redTulip1: `${ASSETS_URL}Bunga Tulip Merah.webp`,
  redTulip2: `${ASSETS_URL}Bunga Tulip Merah 2.webp`,
  star1: `${ASSETS_URL}Bintang 4 - Biru Pink.webp`,
  star2: `${ASSETS_URL}Bintang 4 - kuning biru.webp`,
  blueTulip: `${ASSETS_URL}Bunga Tulip Biru.webp`,
  wing: `${ASSETS_URL}WISJUL VISTOCK6.webp`,
  sun2: `${ASSETS_URL}sun 9.webp`,
  arrow: `${ASSETS_URL}Vector.png`,
  pansies_km4: `${ASSETS_URL}pansies kuning merah 4.webp`,
  pansies_km5: `${ASSETS_URL}pansies kuning merah 5.webp`,
  pansies_km6: `${ASSETS_URL}pansies kuning merah 6.webp`,
  mini_conf: `${ASSETS_URL}mini confetti 1 1.webp`,
  sun3: `${ASSETS_URL}sun 10.webp`,
  bunga1: `${ASSETS_URL}Bunga 1.webp`,
  bunga2: `${ASSETS_URL}Bunga 2.webp`,
  festive3: `${ASSETS_URL}festive3.webp`,
  medpar: [p1, p2, p3, p4, p5, p6, p7],
};

export default images;

// const images = {
//     pansies_km4: pansies_km4,
//     pansies_km5: pansies_km5,
//     pansies_km6: pansies_km6,
//     mini_conf: mini_conf,
//     sun3:sun3,
//     bunga1:bunga1,
//     bunga2: bunga2,
//     festive3:festive3,
//     ornamentTopmost:ornamentTopmost,
//     ornamentTopmid:ornamentTopmid,
//     ornamentBottommid:ornamentBottommid,
//     ornamentBottommost: ornamentBottommost,
//     graphics1:graphics1,
//     festive1:festive1,
//     festive2:festive2,
//     logoLockup:logoLockup,
//     sun:sun,
//     stars1: stars1,
//     stars2: stars2,
//     redTulip1: redTulip1,
//     redTulip2: redTulip2,
//     star1: star1,
//     star2: star2,
//     blueTulip:blueTulip,
//     sun2:sun2,
//     wing:wing,
//     arrow:arrow,
// }
