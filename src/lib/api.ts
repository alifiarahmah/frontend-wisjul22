import axios from 'axios';

// base url BE
export const baseURL =
  import.meta.env.VITE_BACKEND_URL || 'https://api.paradewisudaitb.com';

const api = axios.create({
  baseURL,
  headers: { 'Content-Type': 'application/json' },

  withCredentials: true,
});

// TODO: bikin interceptor buat nambahin credentials

export default api;
