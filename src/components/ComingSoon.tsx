import { Box, Flex, Heading, Image, Text } from '@chakra-ui/react';
import liquid_1 from '../assets/images/vistock/liquid_1.png';
import bg_coming_soon from '../assets/images/bg_coming_soon.jpg';
import Layout from './layout/Layout';

function ComingSoon() {
  return (
    <Layout>
      <Box
        height="100vh"
        backgroundImage={`url(${bg_coming_soon})`}
        backgroundRepeat="no-repeat"
        bgColor="krem.2"
        bgSize="cover"
        bgPosition="bottom"
      >
        <Flex
          height="inherit"
          alignItems="center"
          justifyContent={{ base: 'center', lg: 'flex-start' }}
          fontFamily="heading"
        >
          <Heading
            m={10}
            ml={{ lg: '10rem' }}
            color="bg.dark"
            fontSize={{
              base: '6xl',
              sm: '7xl',
              lg: '8xl',
            }}
            textShadow="dark-lg"
            textAlign="center"
          >
            Coming Soon!
          </Heading>
        </Flex>
      </Box>
    </Layout>
  );
}

export default ComingSoon;
