import styled from '@emotion/styled';
import React from 'react';
import logo_gelap from '../assets/images/logo/logo_gelap.png';
import { Heading, Image } from '@chakra-ui/react';
import { css, keyframes } from '@emotion/react';

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;

  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
`;

const spin = keyframes`
  0% {
    transform: rotate(0deg);
  }
  100% {
    transform: rotate(-360deg);
  }
`;

export default function Loading(): JSX.Element {
  return (
    <Wrapper>
      <Image loading="lazy"
        width={{
          base: '50vw',
          sm: '25vw',
        }}
        transformOrigin={{
          base: 'calc(50% + 5vw) calc(50% + 2vw)',
          sm: 'calc(50% + 2vw) calc(50% + 1vw)',
        }}
        css={css`
          aspect-ratio: 1 / 1;
          animation: ${spin} 1.25s linear infinite;
        `}
        src={logo_gelap}
        alt="Loading..."
      />
      <Heading
        as="h1"
        mt="clamp(1rem, 1rem + 1vw, 2rem)"
        fontSize={{ base: '7vw', sm: '4vw' }}
      >
        Loading...
      </Heading>
    </Wrapper>
  );
}
