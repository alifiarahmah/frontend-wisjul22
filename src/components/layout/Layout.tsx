import { Box } from "@chakra-ui/react";
import Navbar from "../Navbar";
import bgkrem from "../../assets/images/bg_tile_krem.jpg";
import bgpink from "../../assets/images/bg_tile_pink.jpg";
import bgdblue from "../../assets/images/bg_tile_darkblue.jpg";
import bglblue from "../../assets/images/bg_tile_lightblue.jpg";
import bgdgreen from "../../assets/images/bg_tile_darkgreen.jpg";
import bglgreen from "../../assets/images/bg_tile_lightgreen.jpg";
import Footer from "../Footer";
import { Helmet } from "react-helmet";

Layout.defaultProps = {
	navbarVar: "krem",
	footerBg: "light",
	noFooter: false,
	noNavbar: false,
}

function Layout(props: any) {
	// PROPS
	// navbarVar: "blue" | "krem" | "blue transparent" | "krem transparent" | "transparent". default: "krem"
	// bg: "krem" | "pink" | "darkblue" | "lightblue" | "darkgreen" | "lightgreen". default: "krem"
	// footerBg: "dark" | "light" | "none". default: "light"
	// footerTextColor: color. default: "bg.light" if footerBg = "dark" else "bg.dark"
	return (
        <Box
            minHeight="100vh"
            bgImage={
                props.bg === "pink"
                    ? `url(${bgpink})`
                    : props.bg === "darkblue"
                    ? `url(${bgdblue})`
                    : props.bg === "lightblue"
                    ? `url(${bglblue})`
                    : props.bg === "darkgreen"
                    ? `url(${bgdgreen})`
                    : props.bg === "lightgreen"
                    ? `url(${bglgreen})`
                    : `url(${bgkrem})`
            }
            bgColor="bg.light"
        >
            <Helmet>
                <meta charSet="utf-8" />
                <title>
                    {props.title
                        ? `${props.title} | Parade Wisuda Juli ITB 2022`
                        : `Parade Wisuda Juli ITB 2022`}
                </title>
            </Helmet>
            <Navbar
                show={!props.noNavbar}
                krem={!props.navbarVar.includes("blue")}
                noBg={props.navbarVar.includes("transparent")}
            />
            {props.children}
            <Footer
                show={!props.noFooter}
                bg={props.footerBg}
                textColor={
                    props.footerTextColor
                        ? props.footerTextColor
                        : props.footerBg == "dark"
                        ? "bg.light"
                        : "bg.dark"
                }
            />
        </Box>
    );
}

export default Layout;
