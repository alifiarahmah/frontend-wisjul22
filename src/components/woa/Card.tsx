import { Box, Button, Heading, Image, Skeleton, Text } from '@chakra-ui/react';
import { css } from '@emotion/react';
import React from 'react';
import { Link } from 'react-router-dom';
import SekeletonImg from '../SekeletonImg';

interface CardProps {
  id: number;
  title: string;
  nimJurusan: number;
  body: string | JSX.Element | JSX.Element[];
  imgSrc: string;
  imgAlt?: string;
}

export default function Card({
  id,
  title,
  nimJurusan,
  body,
  imgSrc,
  imgAlt = '',
}: CardProps): JSX.Element {
  return (
    <Box
      as="article"
      borderRadius="1em"
      bg="bg.light"
      // height="25em"
      width={{ base: '12.5em', md: '16.25em' }}
      py="1em"
      px="1.75em"
      display="flex"
      flexDir="column"
      css={css`
        box-shadow: 0px 4px 4px 0px #0000000d, 0px 0px 1px 0px #0000001a;
        aspect-ratio: 13 / 20;
      `}
    >
      <Box as="header" mt="0.125em">
        <SekeletonImg
          src={imgSrc}
          alt={imgAlt}
          mx="auto"
          css={css`
            aspect-ratio: 1 / 1;
          `}
          width={{ base: '4.75em', md: '6em' }}
          rounded="full"
          objectFit="cover"
        />

        <Heading
          as="h3"
          fontSize={{ base: '1em', md: '1.5em' }}
          mt="0.75em"
          textAlign="center"
          noOfLines={3}
        >
          {title}
        </Heading>
        <Heading
          as="h5"
          fontSize={{ base: '0.75em', md: '1em' }}
          mt="0.375em"
          textAlign="center"
        >
          {nimJurusan}
        </Heading>
      </Box>

      <Box
        as="main"
        mt="1em"
        css={css`
          flex: 1;
        `}
      >
        <Text noOfLines={4} fontSize={{ base: 'xs', md: 'sm' }}>
          {body}
        </Text>
      </Box>

      <Box as="footer" textAlign="center" mt="1em">
        <Link to={String(id)}>
          <Button
            bgColor="blue.dark.default"
            _hover={{ bg: 'blue.dark.hover' }}
            _active={{
              bg: 'blue.dark.active',
              transform: 'scale(0.95)',
            }}
            fontSize={{ base: 'sm', md: 'md' }}
            mx="auto"
            py="0.5em"
            px="1.25em"
            rounded="full"
          >
            Read More
          </Button>
        </Link>
      </Box>
    </Box>
  );
}