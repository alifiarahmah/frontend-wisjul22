import { Box, Image } from "@chakra-ui/react";
import { useRef } from "react";
import style from "./Photobooth.module.css";
import Webcam from "react-webcam";
import imgs from "../../assets/images/photobooth";

export default function Photobooth() {
  const camRef = useRef(null);

  function captureHandler(e: any) {
    e.preventDefault();
  }
  return (
    <Box className={style.wrapper}>
      <Box className={style.container}>
        <Box className={style.photobooth}>PHOTOBOOTH</Box>
        <Image alt="" loading="lazy" src={imgs.star} className={style.star} />
        <Image alt="" loading="lazy" src={imgs.liquid} className={style.liquid} />
        <Box className={style.webcamWrapper}>
          <Webcam ref={camRef} className={style.webcam} />
        </Box>
      </Box>
    </Box>
  );
}
