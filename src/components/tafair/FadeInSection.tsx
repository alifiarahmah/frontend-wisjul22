import React, { FC, useEffect } from "react";
import { motion,useAnimation } from "framer-motion";
import { useInView } from "react-intersection-observer";

interface Props {
    children: React.ReactNode;
}

const FadeInSection: FC<Props> = ({ children }) => {
    const BoxVariants = {
        visible: { opacity: 1, y: 0, transition: { duration: 2.5, staggerChildren: 3 } },
        hidden: { opacity: 0, y:24 }
    };
    const controls = useAnimation();
    const [ref, inView] = useInView();

    useEffect(() => {
        if (inView) {
            controls.start("visible");
        }
    }, [controls, inView]);

    return (
        <motion.div
            ref={ref}
            animate={controls}
            initial="hidden"
            variants={BoxVariants}
        >
            {children}
        </motion.div>
        
    );
};

export default FadeInSection