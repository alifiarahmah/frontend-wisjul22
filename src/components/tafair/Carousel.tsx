import React, { FC, useState } from 'react';
import { Flex, IconButton, Image } from '@chakra-ui/react';
import { ArrowBackIcon, ArrowForwardIcon } from '@chakra-ui/icons';

const Carousel: FC<{
    imgLinks: string[];
}> = ({ imgLinks }) => {
    const [curIdx, setCurIdx] = useState(0);

    return (
        <>
            <Flex
                flexDir="row"
                gap="4"
                alignItems="center"
                justifyContent="center"
                mt="4"
                fontSize="lg"
                pos="relative"
                width="full"
            >
                <IconButton
                    textColor="blue.dark.default"
                    bgColor="krem.2"
                    aria-label="back"
                    icon={< ArrowBackIcon />}
                    borderRadius="100%"
                    width="50px"
                    height="50px"
                    fontSize="20px"
                    onClick={() =>
                        setCurIdx((curIdx - 1) < 0 ? imgLinks.length - 1 : curIdx - 1)
                    }
                />
                {imgLinks.map((imgLink, idx) => (
                    <Image
                        display={curIdx == idx ? "block" : "hidden"}
                        src={imgLink}
                        rounded="lg"
                        pos="relative"
                        alt={`Foto ke-${idx}`}
                        key={idx}
                        width={{ base: "80vw", md: "550px" }}
                        height={{ base: "40vw", md: "400px" }}
                        objectFit="cover"
                    />
                ))} 
                <IconButton
                    textColor="blue.dark.default"
                    bgColor="krem.2"
                    aria-label="Forward"
                    icon={< ArrowForwardIcon />}
                    borderRadius="100%"
                    width="50px"
                    height="50px"
                    fontSize="20px"
                    onClick={() =>
                        setCurIdx((curIdx + 1) % imgLinks.length)}
                />
            </Flex>
        </>
    )
};

export default Carousel;