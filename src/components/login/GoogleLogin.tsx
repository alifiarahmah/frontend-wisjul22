import { Box, type BoxProps } from "@chakra-ui/react";
import { useContext, useEffect, useRef } from "react";
import GSIScriptContext from "../../context/GSIScriptContext";

export interface GoogleLoginProps extends BoxProps {
  onLogin: (handleCredentialResponse: CredentialResponse) => void;
}

export default function GoogleLogin({ onLogin, ...props }: GoogleLoginProps) {
  const boxRef = useRef<HTMLDivElement>(null);
  const scriptLoaded = useContext(GSIScriptContext);

  useEffect(() => {
    if (typeof window === "undefined" || !window.google || !boxRef.current) {
      return;
    }

    try {
      window.google.accounts.id.initialize({
        client_id: import.meta.env.VITE_GSI_CLIENT as string,
        callback: onLogin,
      });

      window.google.accounts.id.renderButton(boxRef.current, {
        theme: "outline",
        type: "standard",
        shape: "pill",
        text: "signin_with",
        size: "large",
        logo_alignment: "left",
      });
    } catch (err) {
      console.error(`Can't load google login button : ${err}`);
    }
  }, [scriptLoaded, onLogin]);

  return <Box {...props} ref={boxRef}></Box>;
}
