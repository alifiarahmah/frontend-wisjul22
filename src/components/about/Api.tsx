import React from "react";
import { Box, Flex, Heading, Image, Text } from "@chakra-ui/react";
import api from "../../assets/images/about/api.png";
import getRawDrive from "../../lib/getRawDrive";

export interface ApiProps {
  nama: string;
  jurusan: string;
  jabatan: string;
  img: string;
  key: string;
}

export default function Api({ nama, jurusan, jabatan, img, key }: ApiProps) {
  return (
    <Box my={5} mx={10} w="56">
      <Box
        w="inherit"
        h="280"
        bgColor="bg.light"
        bgImage={`url(${getRawDrive(img)})`}
        bgPos="top"
        bgSize="cover"
        rounded={26}
        marginBottom="20"
        position="relative"
      >
        <Image
          loading="lazy"
          src={api}
          alt=""
          transform="scaleX(-1)"
          width="36"
          position="absolute"
          bottom="-10"
          left="-12"
        />
        <Image
          loading="lazy"
          src={api}
          alt=""
          width="36"
          position="absolute"
          bottom="-10"
          right="-12"
        />
      </Box>
      <Box mt={-10} textAlign="center" mx={2}>
        <Heading fontSize="xl" fontWeight="bold" marginBottom={2}>
          {nama} <br /> ({jurusan})
        </Heading>
        <Text fontSize="sm">{jabatan}</Text>
      </Box>
    </Box>
  );
}
