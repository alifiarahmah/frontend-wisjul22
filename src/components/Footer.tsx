import { Box, Show } from "@chakra-ui/react";

/*

! <Footer dark /> untuk footer background gelap
! <Footer /> untuk footer background terang

*/

interface footerProps {
  bg: string;
  textColor: string;
  show: boolean;
}

Footer.defaultProps = {
  bg: "dark",
  show: true,
};

function Footer(props: footerProps) {
  return (
    <Box
      display={props.show ? "flex" : "none"}
      opacity={props.show ? "100%" : "0%"}
      position={"absolute"}
      top={"100%"}
      bottom={"0"}
      width={"100%"}
      minH={"3.5rem"}
      bg={props.bg == "none" ? "transparent" : ((props.bg == "dark") ? "bg.dark" : "bg.light")}
      fontSize={{
        base: "12px",
        sm: "14px",
        md: "16px",
        lg: "18px",
      }}
      fontWeight={"bold"}
      color={props.textColor}
      alignItems={"center"}
      justifyContent={"center"}
      boxShadow={"lg"}
      zIndex={"99"}
    >
      <p>Copyright &copy; 2022 Parade Wisuda Juli</p>
    </Box>
  );
}

export default Footer;
