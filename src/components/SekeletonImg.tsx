import { Image, Skeleton, SkeletonProps, forwardRef } from '@chakra-ui/react';
import { css } from '@emotion/react';
import React from 'react';

interface SekeletonImgProps extends SkeletonProps {
  src: string;
  alt?: string;
}

export default forwardRef<SekeletonImgProps, 'div'>(function SekeletonImg(
  { src, alt = '', ...rest },
  ref
): JSX.Element {
  const [loaded, setLoaded] = React.useState(false);

  return (
    <Skeleton
      // props css dan width sebagai default width ketika tidak ditentukan
      css={
        !rest.width &&
        !rest.height &&
        css`
          aspect-ratio: 16 / 9;
        `
      }
      width={loaded ? '' : '70vw'}
      // props css dan width dapat ditimpa melalui props dibawah
      {...rest}
      ref={ref}
      isLoaded={loaded}
    >
      <Image
        loading="lazy"
        src={src}
        alt={alt}
        {...rest}
        onLoad={() => setLoaded(true)}
      />
    </Skeleton>
  );
});
