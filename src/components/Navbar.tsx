import {
  Box,
  Flex,
  Image,
  Menu,
  MenuButton,
  MenuList,
  MenuItem,
  Text,
} from "@chakra-ui/react";
import { Link } from "react-router-dom";
import { useState, useEffect } from "react";
import "./Navbar.css";

// Import assets
import logo from "../assets/images/logo/logo_border.png";
import { HamburgerIcon } from "@chakra-ui/icons";

interface navbarProps {
  krem: boolean;
  noBg: boolean;
  show: boolean;
}

Navbar.defaultProps = {
  krem: false,
  noBg: false,
  show: true,
};

/*

! <Navbar /> untuk navbar warna biru
! <Navbar krem /> untuk navbar warna krem
! <Navbar noBg /> untuk navbar tanpa background

*/

function Navbar(props: navbarProps) {
  // Variable definition

  // Breakpoint size in pixels
  const mobileSize = 768; // medium

  // Theming
  let colors = {
    primary: {
      satu: "var(--chakra-colors-bg-dark)",
      dua: "var(--chakra-colors-blue-dark-default)",
    },
    secondary: {
      satu: "var(--chakra-colors-krem-1)",
      dua: "var(--chakra-colors-krem-2)",
    },
  };

  if (props.krem) {
    colors.primary.satu = "var(--chakra-colors-krem-1)";
    colors.primary.dua = "var(--chakra-colors-krem-2)";
    colors.secondary.satu = "var(--chakra-colors-bg-dark)";
    colors.secondary.dua = "var(--chakra-colors-blue-dark-default)";
  }

  // Detect when screen size has changed
  function getWindowDimensions() {
    const { innerWidth: width } = window;
    return {
      width,
    };
  }

  function useWindowDimensions() {
    const [windowDimensions, setWindowDimensions] = useState(
      getWindowDimensions()
    );

    useEffect(() => {
      function handleResize() {
        setWindowDimensions(getWindowDimensions());
      }

      window.addEventListener("resize", handleResize);
      return () => window.removeEventListener("resize", handleResize);
    }, []);

    return windowDimensions;
  }

  const { width } = useWindowDimensions();

  if (width >= mobileSize) {
    /*  Desktop view  */
    return (
      <Flex
        className={"nav"}
        display={props.show ? "flex" : "none"}
        backgroundColor={props.noBg ? "transparent" : colors.primary.satu}
        boxShadow={
          props.noBg
            ? "null"
            : "rgba(0, 0, 0, 0.07) 0px 1px 1px, rgba(0, 0, 0, 0.07) 0px 2px 2px, rgba(0, 0, 0, 0.07) 0px 4px 4px, rgba(0, 0, 0, 0.07) 0px 8px 8px, rgba(0, 0, 0, 0.07) 0px 16px 16px;"
        }
      >
        <Flex className={"nav-container"}>
          <Link to="/">
            <Flex className={"logo"}>
              <Image alt="" loading="lazy" className={"logo-img"} src={logo} />
              <Box className={"logo-text"} color={colors.secondary.dua}>
                Wisuda Juli&nbsp;
                <span className={props.krem ? "text-color-1" : "text-1"}>
                  2
                </span>
                <span className={props.krem ? "text-color-2" : "text-2"}>
                  0
                </span>
                <span className={props.krem ? "text-color-3" : "text-3"}>
                  2
                </span>
                <span className={props.krem ? "text-color-4" : "text-4"}>
                  2
                </span>
              </Box>
            </Flex>
          </Link>
          <Flex className={"btn-container"}>
            <Box
              className="btn"
              as="button"
              backgroundColor={colors.secondary.satu}
              color={colors.primary.satu}
              _hover={{
                backgroundColor: colors.secondary.dua,
                boxShadow: "rgba(0, 0, 0, 0.15) 0px 5px 15px 0px;",
              }}
            >
              <Link to="/wisudawan">Graduates</Link>
            </Box>
            <Link to="/about">
              <Text color={colors.secondary.satu}>About Us</Text>
            </Link>
          </Flex>
        </Flex>
      </Flex>
    );
  } else {
    /* Mobile view */
    return (
      <Flex
        display={props.show ? "flex" : "none"}
        className={"nav"}
        backgroundColor={props.noBg ? "transparent" : colors.primary.satu}
        boxShadow={
          props.noBg
            ? "null"
            : "rgba(0, 0, 0, 0.07) 0px 1px 1px, rgba(0, 0, 0, 0.07) 0px 2px 2px, rgba(0, 0, 0, 0.07) 0px 4px 4px, rgba(0, 0, 0, 0.07) 0px 8px 8px, rgba(0, 0, 0, 0.07) 0px 16px 16px;"
        }
      >
        <Flex className={"nav-container"}>
          <Flex className={"logo-img-container"}>
            <Link to="/">
              <Image alt="" loading="lazy" className={"logo-img"} src={logo} />
            </Link>
          </Flex>
          <Flex className={"logo-text"} color={colors.secondary.dua}>
            Wisuda Juli&nbsp;
            <span className={props.krem ? "text-color-1" : "text-1"}>2</span>
            <span className={props.krem ? "text-color-2" : "text-2"}>0</span>
            <span className={props.krem ? "text-color-3" : "text-3"}>2</span>
            <span className={props.krem ? "text-color-4" : "text-4"}>2</span>
          </Flex>
          <Flex className={"btn-mobile-container"}>
            <Menu>
              <MenuButton className="menu-btn">
                <HamburgerIcon
                  className="icon-menu"
                  boxSize={"60%"}
                  color={colors.secondary.satu}
                />
              </MenuButton>
              <MenuList>
                <MenuItem>
                  <Link to="/wisudawan">Graduates</Link>
                </MenuItem>
                <MenuItem>
                  <Link to="/about">About Us</Link>
                </MenuItem>
              </MenuList>
            </Menu>
          </Flex>
        </Flex>
      </Flex>
    );
  }
}

export default Navbar;
