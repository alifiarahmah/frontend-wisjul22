import { Box, Flex, Heading, Image, Text } from "@chakra-ui/react";
import style from "./Landing.module.css";
import imgs from "../../assets/images/landing";
import Graphics1 from "./Parts/Graphics1";
import Ornament1 from "./Parts/Ornament1";
import Ornament2 from "./Parts/Ornament2";
import config from "./config";
import Ornament3 from "./Parts/Ornament3";
import Ornament4 from "./Parts/Ornament4";
import { useEffect, useState } from "react";
import Descriptions from "./Parts/Descriptions";
import Graphics2 from "./Parts/Graphics2";
import Explore from "./Parts/Explore";
import Sponmed from "./Parts/Sponmed";

export default function Landing() {
  const [daysleft, setDaysleft] = useState(0);
  const [visitTime, setVisittime] = useState(0);

  useEffect(() => {
    const theDateinms = Date.parse(config.theDate)
    const curDateinms = Date.parse(Date())
    const Dleft = Math.ceil((theDateinms - curDateinms) / 86400000)
    setDaysleft(Dleft > 0 ? Dleft : 0);
  });

  return (
    <Box className={style.container}>
      <Ornament1 />

      <Graphics1 />

      <Graphics2 />

      {/* <Ornament2 /> */}

      {/* <Box className={style["trailer-container"]}>
        <h1 className={style["trailer-header"]}>UPCOMING</h1>
        <iframe
          src={config.trailerLink}
          title="OSKM ITB 2021 Official Teaser 2"
          id="trailer"
          allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
        ></iframe>
      </Box> */}

      <Ornament3 />

      <Descriptions />

      <Ornament4 daysleft={daysleft} />
      <Box className={style.darkbg}> 
      <Explore/>

      <Sponmed/>
      </Box>
    </Box>
  );
}
