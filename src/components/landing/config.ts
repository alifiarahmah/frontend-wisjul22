import { MdLocalLibrary, MdPeople, MdSchool } from "react-icons/md"
import { IoMdTrophy } from "react-icons/io"

const config = {
    exploreConfig: [
        {
            title: "ABOUT US",
            desc: "Kenalan dulu sama visi, misi, dan orang-orang keren di balik Parade Wisuda ITB Juli 2022.",
            btn: "Tentang kami",
            bicon: MdPeople,
            link: "/about"
        },
        // {
        //     title: "Events",
        //     desc: "Ada acara apa aja ya, di parade wisuda kali ini?",
        //     btn: "Lihat events",
        //     bicon: MdToday,
        //     link: ""
        // },
        {
            title: "WISUDAWAN",
            desc: "Nyari profil kamu, temen kamu, atau gebetan kamu? Di sini tempatnya.",
            btn: "Galeri wisudawan",
            bicon: MdSchool,
            link: "/wisudawan"
        },
        // {
        //     title: "PHOTOBOOTH",
        //     desc: "Kalo kata anak jaman sekarang sih, twibbon",
        //     btn: "Foto",
        //     bicon: MdToday,
        //     link: "/photobooth"
        // },
        // {
        //     title: "IEU NAON",
        //     desc: "ADA APA AJA SIH",
        //     btn: "?",
        //     bicon: MdToday,
        //     link: ""
        // },
        // {
        //     title: "?",
        //     desc: "?",
        //     btn: "?",
        //     bicon: MdToday,
        //     link: ""
        // }
        {
            title: "TA FAIR",
            desc: "Yuk kita lihat hasil jerih payah perjuangan para wisudawan.",
            btn: "TA Fair",
            bicon: MdLocalLibrary,
            bg: '#FFFFFF',
            link: "/ta-fair" 
        },
        {
            title: "Wall of Appreciation",
            desc: "Kisah inspiratif para wisudawan",
            btn: "Lihat kisah",
            bicon: IoMdTrophy,
            bg: '#FFFFFF',
            link: "/woa" 
        }
    ],
    theDate: "2022-07-23T07:00:00+07:00",
    trailerLink: "https://www.youtube.com/embed/qdUUBlaEfH8?autoplay=1&mute=1",
    text1: "“Here Comes The Sun”",
    text2: "Adalah sebuah sarana dan wadah apresiasi bagi wisudawan yang telah melalui masa-masa sulit dan kelam layaknya awan mendung di tengah badai. Namun, semua itu sirna oleh semangat dan gairah yang mereka miliki dalam usaha menggapai mimpi yang menjelma menjadi cahaya terang. Mereka membawa harapan masa depan yang cerah layaknya matahari terbit yang menghalau gelapnya hujan. Bersama-sama, wisudawan, massa kampus, panitia, dan masyarakat, berkumpul sambil menengadah ke arah timur, merayakan timbulnya mentari dalam perayaan Wisuda Juli 2022."
}



export default config