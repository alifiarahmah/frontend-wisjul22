import { Box } from "@chakra-ui/react";
import imgs from "../../../assets/images/landing/index";
import style from "./Graphics1.module.css";

export default function Graphics1() {
  return (
    <Box className={style["graphics1-container"]}>
        <img alt="" className={style.festive1} src={imgs.festive1} />
        <img alt="" className={style.festive2} src={imgs.festive2} />
        <img alt="" className={style.logoLockup} src={imgs.logoLockup} />
    </Box>
  );
}
