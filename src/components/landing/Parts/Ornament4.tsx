import style from "./Ornament4.module.css";
import imgs from "../../../assets/images/landing/index";
import { theme } from "../../../theme";

import { Box, Image } from "@chakra-ui/react";

export default function Ornament4(props: { daysleft: number }) {
  return (
    <Box className={style.container}>
      <Image alt="" loading="lazy"
        className={style.ornamentbottommost}
        src={imgs.ornamentBottommost}
      />
      <Image alt="" loading="lazy"
        className={style.festive3}
        src={imgs.festive3}
      />

      <Box className={style["countdown-container"]}>
        <Box className={style["countdowns"]}>
          <div className={style.cd}>{props.daysleft}</div>
          <div className={style.hari}>hari</div>
          <div className={style.menuju}>Menuju Parade Wisuda ITB</div>
        </Box>
      </Box>
    </Box>
  );
}
