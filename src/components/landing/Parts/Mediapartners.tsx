import { Box, Image } from "@chakra-ui/react";
import style from "./Mediapartners.module.css";
import imgs from "../../../assets/images/landing/index"

export default function Mediapartners() {
  return (
    <Box className={style.container}>
      <Box className={style.heads}>
        <Image alt="" loading="lazy" src={imgs.star1} className={style.star} />
        <Box className={style.mediapartners}>Media Partners</Box>
        <Image alt="" loading="lazy" src={imgs.star1} className={style.star} />
      </Box>
    </Box>
  );
}
