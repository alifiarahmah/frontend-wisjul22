import style from "./Explore.module.css";
import imgs from "../../../assets/images/landing";
import { Box, Image } from "@chakra-ui/react";
import config from "../config";
import { Link } from "react-router-dom";
import { IconContext } from "react-icons";

export default function Explore() {
 

  /* creating explore contents */
  const exploreItems = config.exploreConfig.map((e) => (
    <Box className={style.exploreItem}>
      <Box className={style.exploreItemWrapper}>
        <Image alt="" loading="lazy" className={style.icon} src={imgs.blueTulip} />
        <h1 className={style.title}>{e.title}</h1>
        <p className={style.desc}>{e.desc}</p>
        <Link to={e.link || "/"} className={style.btn} onClick={() => window.scrollTo(0,0)}>
          {e.btn}
          <e.bicon className={style.mdi}/>
        </Link>
      </Box>
    </Box>
  ));

  return (
    <Box className={style.container} id="explore">
      <Box className={style.titleWrapper}>
        <Image alt="" loading="lazy" className={style.wingLeft} src={imgs.wing} />
        <h1>EXPLORE</h1>
        <Image alt="" loading="lazy" className={style.wingRight} src={imgs.wing} />
      </Box>
      <IconContext.Provider value={{color: "#ffffff"}}>
        <Box className={style.exploreItemContainer}>{exploreItems}</Box>
      </IconContext.Provider>
    </Box>
  );
}
