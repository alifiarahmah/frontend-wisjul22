import style from "./Ornament2.module.css"
import imgs from "../../../assets/images/landing/index"

import { Box, Image} from "@chakra-ui/react";

export default function Ornament2() {
    return <Box className={style.container}>
      
        <Image alt="" loading="lazy"
          className={style["ornament-topmid"]}
          src={imgs.ornamentTopmid}
        />
        <Image alt="" loading="lazy"
          className={style["sun"]}
          src={imgs.sun}
        />

        <Image alt="" loading="lazy" className={style["pansies_km4"]} src={imgs.pansies_km4}/>
        <Image alt="" loading="lazy" className={style["pansies_km5"]} src={imgs.pansies_km5}/>
        <Image alt="" loading="lazy" className={style["pansies_km6"]} src={imgs.pansies_km6}/>

    </Box>
}