import { Box, Image } from "@chakra-ui/react";
import style from "./Graphics2.module.css";
import config from "../config";
import imgs from "../../../assets/images/landing/index";
import { Link } from "react-router-dom";

export default function Graphics2() {
  function exploreBtnHandler() {
    document.getElementById("explore")?.scrollIntoView({ behavior: "smooth" });
  }

  return (
    <Box className={style["container"]}>
      <Box className={style["container-top"]}>
        <p>{config.text1}</p>

        <Box className={style["btn-container"]}>
          <Box className={style["btn-wrapper"]}>
            <Link to="/events">
              <button className={style["btn-1"]}>Events</button>
            </Link>
          </Box>
          <Box className={style["btn-wrapper"]} onClick={exploreBtnHandler}>
            <button className={style["btn-2"]}>
              Explore <Image alt="" loading="lazy" width="1em" src={imgs.arrow} />
            </button>
          </Box>
        </Box>
      </Box>
    </Box>
  );
}
