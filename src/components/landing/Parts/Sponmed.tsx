import style from "./Sponmed.module.css";
import { Box, Image } from "@chakra-ui/react";
import imgs from "../../../assets/images/landing/index";
import Medpar from "../../../assets/images/landing/Medpar.png"

export default function Sponmed() {
  return (
    <Box className={style.container}>
      {/* <Box className={style["part-container"]}>
        <Box className={style.heads}>
          <Image alt="" loading="lazy" src={imgs.star2} className={style.star} />
          <Box className={style.sponsoredby}>Sponsored By</Box>
          <Image alt="" loading="lazy" src={imgs.star2} className={style.star} />
        </Box>
      </Box> */}
      <Box className={style["part-container"]}>
        <Box className={style.heads}>
          <Image alt="" loading="lazy" src={imgs.star2} className={style.star} />
          <Box className={style.sponsoredby}>Media Partners</Box>
          <Image alt="" loading="lazy" src={imgs.star2} className={style.star} />
        </Box>
        {imgs.medpar.map(e => <Image alt="" className={style.png} src={e}/>)}
      </Box>
    </Box>
  );
}
