import style from "./Ornament1.module.css"
import imgs from "../../../assets/images/landing/index"

import { Box, Image} from "@chakra-ui/react";

export default function Ornament1() {
    return <Box className={style.container}>
        <Image alt="" loading="lazy"
          className={style["ornament-topmost"]}
          src={imgs.ornamentTopmost}
        />
    </Box>
}