import style from "./Ornament3.module.css"
import imgs from "../../../assets/images/landing/index"

import { Box, Image} from "@chakra-ui/react";

export default function Ornament3() {
    return <Box className={style.container}>
      
        <Image alt="" loading="lazy"
          className={style.stars1}
          src={imgs.stars1}
        />
        <Image alt="" loading="lazy"
          className={style["bottommid"]}
          src={imgs.ornamentBottommid}
        />
        <Image alt="" loading="lazy"
          className={style["redtulip1"]}
          src={imgs.redTulip1}
        />
        <Image alt="" loading="lazy"
          className={style["redtulip2"]}
          src={imgs.redTulip2}
        />
        <Image alt="" loading="lazy"
          className={style["stars2"]}
          src={imgs.stars2}
        />
        <Image alt="" loading="lazy" 
          className={style["mini_conf"]}
          src={imgs['mini_conf']}
        />
    </Box>
}