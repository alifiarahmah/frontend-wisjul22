import { Box, Image } from "@chakra-ui/react";
import imgs from "../../../assets/images/landing/index";
import style from "./Descriptions.module.css";
import config from "../config";

export default function Descriptions() {
  return (
    <Box className={style["container"]}>
      <Box className={style["sun3-wrapper"]}>
        <Image alt="" loading="lazy" className={style["sun3"]} src={imgs["sun3"]}/>
      </Box>

      <Box className={style["container2"]}>
        <h1 className={style["title"]}>PARADE WISUDA ITB</h1>
        <p className={style["text"]}>{config.text2}</p>
      </Box>

      <Box className={style["bunga1-wrapper"]}>
        <Image alt="" loading="lazy" className={style.bunga1} src={imgs.bunga1}/>
        <Image alt="" loading="lazy" className={style.bunga2} src={imgs.bunga2}/>
      </Box>
    </Box>
  );
}
