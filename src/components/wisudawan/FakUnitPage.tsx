import { Button, Image, Box, Flex, Heading, Text } from "@chakra-ui/react";
import { useNavigate } from "react-router";
import useFetch from "../../hooks/useFetch";
import Loading from "../Loading";

export interface FakUnitPageProps {
	singkatan: string;
	type: "himpunan" | "ukm";
}

function FakUnitPage({
	singkatan = "HMME",
	type = "himpunan",
}: FakUnitPageProps) {
	const urlPath = `${
		import.meta.env.VITE_BACKEND_URL
	}/${type}/${singkatan.toUpperCase()}`;
	const { data, isLoading } = useFetch(urlPath, {});
	const p = (data as any).data;

	const navigate = useNavigate();

	if (isLoading) {
		return <Loading/>;
	}

	if (!data || !(data as any).data) {
		return <div></div>;
	}

	return (
    <Flex
      flexDirection={{ base: 'column', lg: 'row' }}
      px={10}
      justifyContent="center"
      alignItems="center"
    >
      <Box width={{ base: '50%', lg: '20%' }} mx={20}>
    <Image alt=""
          src={
            p[0].logo_url ??
            'https://static.paradewisudaitb.com/assets/images/logo/logo.png'
          }
          loading="lazy"
        />
      </Box>
      <Box width={{ base: '100%', lg: '60%' }}>
        <Heading
          as="h1"
          my={5}
          fontSize={{ base: '3xl', lg: '5xl' }}
          textAlign={{ base: 'center', lg: 'left' }}
        >
          {p[0].nama ?? ''}
        </Heading>
        <Text
          my={5}
          fontWeight="bold"
          textAlign={{ base: 'center', lg: 'left' }}
        >
          {p[0].deskripsi ?? ''}
        </Text>
        <Flex justifyContent={{ base: 'center', lg: 'start' }}>
          <Button my={5} onClick={() => navigate(`/${type}/${singkatan}`)}>
            Lihat Wisudawan
          </Button>
        </Flex>
      </Box>
    </Flex>
  );
}

export default FakUnitPage;
