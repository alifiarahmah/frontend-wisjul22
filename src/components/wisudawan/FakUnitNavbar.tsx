import {
	Button,
	Flex,
	ButtonGroup,
} from "@chakra-ui/react";
import fakultas from "../../assets/json/fakultas.json";

function FakUnitNavbar({
	// viewMode,
	fak,
	kateg,
	// handleViewMode,
	handleSection
}: any) {
	return (
		<>
			{/* Navigation */}
			<Flex
				bg="bg.dark"
				alignItems="center"
				overflowX="scroll"
				sx={{
					"&::-webkit-scrollbar": {
						display: { lg: "none" },
						height: "5px",
						backgroundColor: `bg.light`,
					},
					"&::-webkit-scrollbar-thumb": {
						background: "#555",
						borderRadius: "full"
					},
				}}
			>
				{/* Ada stack + dropdown */}
				{/* <Menu isLazy>
					<MenuButton
						as={Button}
						display="flex"
						alignItems="center"
						rightIcon={<MdArrowDropDown />}
						size="md"
						fontSize="md"
						bg="bg.dark"
						color="white"
						borderRadius="none"
						borderRightWidth="1px"
					>
						{viewMode}
					</MenuButton>
					<MenuList bg="bg.dark" color="white" borderRadius="none">
						<MenuItem onClick={() => handleViewMode("Fakultas")}>
							Fakultas
						</MenuItem>
						<MenuItem onClick={() => handleViewMode("UKM/BSO")}>
							Unit/BSO
						</MenuItem>
					</MenuList>
				</Menu> */}
				{/* fak (fak/ukm) - dropdown */}
				<ButtonGroup
					isAttached
					spacing={1}
					// display={{ base: "none", lg: "flex" }}
				>
					{
						// viewMode === "Fakultas"?
						fakultas.map((f) => {
							return (
								<Button
									key={f.singkatan + "button"}
									size="lg"
									borderRightWidth="1px"
									fontSize="md"
									bg="bg.dark"
									color="white"
									borderRadius="none"
									onClick={() => handleSection(f)}
								>
									{f.singkatan}
								</Button>
							);
						})
						// : units.map((u) => {
						// 		return (
						// 			<Button
						// 				key={u.singkatan}
						// 				size="lg"
						// 				borderRightWidth="1px"
						// 				fontSize="md"
						// 				bg="bg.dark"
						// 				color="white"
						// 				borderRadius="none"
						// 				onClick={() => handleSection(u)}
						// 			>
						// 				{u.singkatan}
						// 			</Button>
						// 		);
						//   })
					}
				</ButtonGroup>
			</Flex>
		</>
	);
}

export default FakUnitNavbar;
