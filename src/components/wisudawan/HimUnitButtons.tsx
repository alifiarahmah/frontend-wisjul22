import { Button } from "@chakra-ui/react";

function HimUnitButtons(props: any) {
  return (
		<Button
			m={1}
			size="lg"
			borderLeftWidth="1px"
			fontSize="md"
			bg="krem.2"
			color="bg.dark"
			fontWeight="bold"
			boxShadow="md"
			borderRadius="full"
			_hover={{
				bg: "bg.dark",
				color: "white",
			}}
			_selected={{
				bg: "bg.dark",
				color: "white",
			}}
			onClick={props.onClick}
			{...props}
		>
			{props.children}
		</Button>
  );
}

export default HimUnitButtons;