import { Box, Flex, Heading, Image, Text } from "@chakra-ui/react";
import { Link } from "react-router-dom";
import bg_NotFound from "../assets/images/bg_NotFound.jpg";
import Layout from "./layout/Layout";

function NotFound() {
    return (
        <Layout navbarVar="blue">
            <Box
                height="100vh"
                backgroundImage={`url(${bg_NotFound})`}
                backgroundSize="cover"
                backgroundColor="bg.dark"
                justifyContent="center"
                alignItems="center"
            >

                <Flex
                    height="inherit"
                    alignItems="center"
                    justifyContent="center"
                    flexDirection="column"
                >
                    <Flex
                        flexDirection="column"
                        mx="10"
                        color="bg.light"
                    >
                        <Heading
                            fontSize="6vw"
                            fontFamily="heading"
                            textShadow="dark-lg"
                            textAlign="left"
                        >
                            LAMAN <br />
                            TIDAK DITEMUKAN
                        </Heading>
                        <Text
                            fontWeight="700"
                            fontSize="2vw"
                            fontFamily="body"
                            textDecoration="underline"
                            textAlign="right"
                            _hover={{ color: "yellow.normal" }}
                        >
                            <Link to="/">
                                kembali ke homepage
                            </Link>
                        </Text>
                    </Flex>
                </Flex>
            </Box>
        </Layout>
    )
}

export default NotFound