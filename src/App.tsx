import { BrowserRouter, Route, Routes } from "react-router-dom";
import ComingSoon from "./components/ComingSoon";
import NotFound from "./components/NotFound";
import Wisudawan from "./pages/Wisudawan/Index";
import PhotoboothPage from "./pages/PhotoboothPage";
import WisudawanProfile from "./pages/Wisudawan/Profile";
import TAFair from "./pages/TAFair/TAFair";
import TAFairAbout from "./pages/TAFair/TAFairAbout";
import TAFairMain from "./pages/TAFair/TAFairMain";
import Exhibition from "./pages/TAFair/Exhibition";
import TA from "./pages/TAFair/TA";
import Landing from './pages/LandingPage';
import ScrollToTop from "./lib/ScrollToTop";
import WisudawanList from "./pages/Wisudawan/List";
import AboutUs from "./pages/AboutUs";
import WOAPage from "./pages/WOAPage";
import WOAById from "./pages/WOAPage/[id]";

function App() {
	return (
		<BrowserRouter>
			<ScrollToTop />
			<Routes>
				<Route path="/" element={<Landing />} />
				<Route path="/about" element={<AboutUs />} />
				<Route path="/photobooth" element={<PhotoboothPage />} />
				{/* add another routes here */}

				{/* Wisudawan */}
				<Route path="/himpunan/:id" element={<WisudawanList />} />
        <Route path="/wisudawan" element={<Wisudawan />} />
        <Route path="/wisudawan/:id" element={<WisudawanProfile />} />

				{/* coming soon */}
				<Route path="/events" element={<ComingSoon />} />
				<Route path="/woa" element={<WOAPage />} />
				<Route path="/woa/:id" element={<WOAById />} />

				{/* TA Fair */}
				<Route path="/ta-fair" element={<TAFair />} />
				<Route path="/ta-fair/about" element={<TAFairAbout />} />
				<Route path="/ta-fair/main" element={<TAFairMain />} />
				<Route path="/ta-fair/exhibition" element={<Exhibition />} />
				<Route path="/ta-fair/tugasakhir/:id" element={<TA />} />

				{/* 404 */}
				<Route path="*" element={<NotFound />} />
			</Routes>
		</BrowserRouter>
	);
}

export default App;
